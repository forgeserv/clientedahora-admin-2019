<?php

Breadcrumbs::for('dashboard', function ($breadcrumb) {
  $breadcrumb->push('Dashboard', route('admin.dashboard.index'));
});

Breadcrumbs::for('audits.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Auditoria', route('admin.audits.index'));
});

Breadcrumbs::for('products.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Produtos', route('admin.products.index'));
});

Breadcrumbs::for('accounts.show', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Meu Perfil', route('admin.accounts.show'));
});

Breadcrumbs::for('accounts.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('accounts.show');
  $breadcrumb->push('Editar Perfil', route('admin.accounts.edit', ['id' => $result->id]));
});

Breadcrumbs::for('audits.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('audits.index');
  $breadcrumb->push('Visualizar Log', route('admin.audits.show', ['id' => $result->id]));
});

Breadcrumbs::for('messages.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Mensagens', route('admin.messages.index'));
});

Breadcrumbs::for('messages.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('messages.index');
  $breadcrumb->push('Visualizar Mensagem', route('admin.messages.show', ['id' => $result->id]));
});

Breadcrumbs::for('users.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Usuários', route('admin.users.index'));
});

Breadcrumbs::for('users.create', function ($breadcrumb) {
  $breadcrumb->parent('users.index');
  $breadcrumb->push('Novo Usuário', route('admin.users.create'));
});

Breadcrumbs::for('users.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('users.index');
  $breadcrumb->push('Editar Usuário', route('admin.users.edit', ['id' => $result->id]));
});

Breadcrumbs::for('roles.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Grupos', route('admin.roles.index'));
});

Breadcrumbs::for('roles.create', function ($breadcrumb) {
  $breadcrumb->parent('roles.index');
  $breadcrumb->push('Novo Grupo', route('admin.roles.create'));
});

Breadcrumbs::for('roles.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('roles.index');
  $breadcrumb->push('Editar Grupo', route('admin.roles.edit', ['id' => $result->id]));
});

Breadcrumbs::for('permissions.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Permissões', route('admin.permissions.index'));
});

Breadcrumbs::for('permissions.create', function ($breadcrumb) {
  $breadcrumb->parent('permissions.index');
  $breadcrumb->push('Nova Permissão', route('admin.permissions.create'));
});

Breadcrumbs::for('permissions.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('permissions.index');
  $breadcrumb->push('Editar Permissão', route('admin.permissions.edit', ['id' => $result->id]));
});

Breadcrumbs::for('departments.index', function ($breadcrumb) {
  $breadcrumb->parent('messages.index');
  $breadcrumb->push('Departamentos', route('admin.departments.index'));
});

Breadcrumbs::for('departments.create', function ($breadcrumb) {
  $breadcrumb->parent('departments.index');
  $breadcrumb->push('Novo Departamento', route('admin.departments.create'));
});

Breadcrumbs::for('departments.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('departments.index');
  $breadcrumb->push('Editar Departamento', route('admin.departments.edit', ['id' => $result->id]));
});

Breadcrumbs::for('units.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Unidades', route('admin.units.index'));
});

Breadcrumbs::for('units.create', function ($breadcrumb) {
  $breadcrumb->parent('units.index');
  $breadcrumb->push('Nova Unidade', route('admin.units.create'));
});

Breadcrumbs::for('units.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('units.index');
  $breadcrumb->push('Editar Unidade', route('admin.units.edit', ['id' => $result->id]));
});

Breadcrumbs::for('awards.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Prêmios', route('admin.awards.index'));
});

Breadcrumbs::for('awards.create', function ($breadcrumb) {
  $breadcrumb->parent('awards.index');
  $breadcrumb->push('Novo Prêmio', route('admin.awards.create'));
});

Breadcrumbs::for('awards.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('awards.index');
  $breadcrumb->push('Editar Prêmio', route('admin.awards.edit', ['id' => $result->id]));
});

Breadcrumbs::for('awards_categories.index', function ($breadcrumb) {
  $breadcrumb->parent('awards.index');
  $breadcrumb->push('Categorias', route('admin.awards_categories.index'));
});

Breadcrumbs::for('awards_categories.create', function ($breadcrumb) {
  $breadcrumb->parent('awards_categories.index');
  $breadcrumb->push('Nova Categoria', route('admin.awards_categories.create'));
});

Breadcrumbs::for('awards_categories.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('awards_categories.index');
  $breadcrumb->push('Editar Categoria', route('admin.awards_categories.edit', ['id' => $result->id]));
});

Breadcrumbs::for('promotions.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Promoções', route('admin.promotions.index'));
});

Breadcrumbs::for('promotions.create', function ($breadcrumb) {
  $breadcrumb->parent('promotions.index');
  $breadcrumb->push('Nova Promoção', route('admin.promotions.create'));
});

Breadcrumbs::for('promotions.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('promotions.index');
  $breadcrumb->push('Editar Promoção', route('admin.promotions.edit', ['id' => $result->id]));
});

Breadcrumbs::for('promotions_categories.index', function ($breadcrumb) {
  $breadcrumb->parent('promotions.index');
  $breadcrumb->push('Categorias', route('admin.promotions_categories.index'));
});

Breadcrumbs::for('promotions_categories.create', function ($breadcrumb) {
  $breadcrumb->parent('promotions_categories.index');
  $breadcrumb->push('Nova Categoria', route('admin.promotions_categories.create'));
});

Breadcrumbs::for('promotions_categories.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('promotions_categories.index');
  $breadcrumb->push('Editar Categoria', route('admin.promotions_categories.edit', ['id' => $result->id]));
});

Breadcrumbs::for('clients.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Clientes', route('admin.clients.index'));
});

Breadcrumbs::for('clients.create', function ($breadcrumb) {
  $breadcrumb->parent('clients.index');
  $breadcrumb->push('Novo Cliente', route('admin.clients.create'));
});

Breadcrumbs::for('clients.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('clients.index');
  $breadcrumb->push('Editar Cliente', route('admin.clients.edit', ['id' => $result->id]));
});
