<?php

/**
 * Admin Routes
 */
Route::prefix('admin')->group(function(){

  /**
  * Public Routes
  */
  Route::middleware('guest.admin')->group(function(){
    // Login
    Route::name('admin.session.login')->get('/acessar', 'Admin\Session\SessionController@showLoginForm');
    Route::name('admin.session.login')->post('/acessar', 'Admin\Session\SessionController@login');
    Route::name('autologin')->get('/autologin/{token}', '\Watson\Autologin\AutologinController@autologin');

    // Social Login
    Route::name('admin.social.redirect')->get('/social/redirect/{provider}', 'Admin\Session\SocialController@redirectToProvider');
    Route::name('admin.social.handle')->get('/social/handle/{provider}', 'Admin\Session\SocialController@handleProviderCallback');

    // Reset Password
    Route::name('admin.password.forgot')->get('/esqueci-minha-senha', 'Admin\Session\ForgotPasswordController@showLinkRequestForm');
    Route::name('admin.password.processForgot')->post('/esqueci-minha-senha', 'Admin\Session\ForgotPasswordController@sendResetLinkEmail');
    Route::name('admin.password.reset')->get('/cadastrar-senha/{token}', 'Admin\Session\ResetPasswordController@showResetForm');
    Route::name('admin.password.processReset')->post('/cadastrar-senha', 'Admin\Session\ResetPasswordController@reset');
  });

  // First access
  Route::name('admin.activation.create')->get('/primeiro-acesso', 'Admin\Session\ActivationController@showActivationForm');
  Route::name('admin.activation.processCreate')->post('/primeiro-acesso', 'Admin\Session\ActivationController@store');

  // Clients
  Route::name('admin.clients.welcome')->get('/bem-vindo/{id}', 'Admin\Clients\ClientController@welcome');

  /**
  * Protected Routes
  */
  Route::middleware(['auth.admin'])->group(function(){

    // Api Routes
    Route::prefix('api')->group(function(){
      // Media
      Route::prefix('media/images')->group(function(){
        Route::name('admin.media.index')->get('/', 'Api\MediaLibrary\MediaLibraryController@index');
        Route::name('admin.media.store')->post('/adicionar', 'Api\MediaLibrary\MediaLibraryController@store');
      });
    });

    // Logout
    Route::name('admin.session.logout')->get('/sair', 'Admin\Session\SessionController@logout');

    // Check first access
    Route::middleware(['first.access'])->namespace('Admin')->group(function() {

      // Dashboard
      Route::name('admin.dashboard.index')->get('/', 'Dashboard\DashboardController@index');

      // Account
      Route::prefix('perfil')->group(function() {
        Route::name('admin.accounts.show')->get('/', 'Users\AccountController@show');
        Route::name('admin.accounts.edit')->get('/atualizar', 'Users\AccountController@edit');
        Route::name('admin.accounts.update')->put('/atualizar', 'Users\AccountController@update');
      });

      // Orders
      Route::prefix('pedidos')->middleware('cors')->group(function() {
        Route::name('admin.orders.index')->get('/', 'Orders\OrderController@index');
      });

      // Releases
      Route::prefix('destaques')->group(function() {
        Route::name('admin.releases.index')->get('/', 'Releases\ReleaseController@index');
        Route::name('admin.releases.create')->get('/adicionar', 'Releases\ReleaseController@create');
        Route::name('admin.releases.store')->post('/adicionar', 'Releases\ReleaseController@store');
        Route::name('admin.releases.edit')->get('/editar/{id}', 'Releases\ReleaseController@edit');
        Route::name('admin.releases.update')->put('/editar/{id}', 'Releases\ReleaseController@update');
      });

      // Clients
      Route::prefix('clientes')->group(function() {
        Route::name('admin.clients.index')->get('/', 'Clients\ClientController@index');
        Route::name('admin.clients.show')->get('/visualizar/{id}', 'Clients\ClientController@show');
        Route::name('admin.clients.create')->get('/adicionar', 'Clients\ClientController@create');
        Route::name('admin.clients.store')->post('/adicionar', 'Clients\ClientController@store');
        Route::name('admin.clients.edit')->get('/editar/{id}', 'Clients\ClientController@edit');
        Route::name('admin.clients.update')->put('/editar/{id}', 'Clients\ClientController@update');
        Route::name('admin.clients.destroy')->delete('/deltar/{id}', 'Clients\ClientController@destroy');
      });

      // Promotions
      Route::prefix('promocoes')->group(function() {
        Route::name('admin.promotions.index')->get('/', 'Promotions\PromotionController@index');
        Route::name('admin.promotions.create')->get('/adicionar', 'Promotions\PromotionController@create');
        Route::name('admin.promotions.store')->post('/adicionar', 'Promotions\PromotionController@store');
        Route::name('admin.promotions.edit')->get('/editar/{id}', 'Promotions\PromotionController@edit');
        Route::name('admin.promotions.update')->put('/editar/{id}', 'Promotions\PromotionController@update');
        Route::name('admin.promotions.destroy')->delete('/excluir/{id}', 'Promotions\PromotionController@destroy');
        Route::name('admin.promotions.reorder')->post('/reordenar', 'Promotions\PromotionController@reorder');

        Route::prefix('categorias')->group(function() {
          Route::name('admin.promotions_categories.index')->get('/', 'Promotions\PromotionCategoryController@index');
          Route::name('admin.promotions_categories.create')->get('/adicionar', 'Promotions\PromotionCategoryController@create');
          Route::name('admin.promotions_categories.store')->post('/adicionar', 'Promotions\PromotionCategoryController@store');
          Route::name('admin.promotions_categories.edit')->get('/editar/{id}', 'Promotions\PromotionCategoryController@edit');
          Route::name('admin.promotions_categories.update')->put('/editar/{id}', 'Promotions\PromotionCategoryController@update');
          Route::name('admin.promotions_categories.destroy')->delete('/excluir/{id}', 'Promotions\PromotionCategoryController@destroy');
          Route::name('admin.promotions_categories.reorder')->post('/reordenar', 'Promotions\PromotionCategoryController@reorder');
        });
      });

      // Awards
      Route::prefix('premios')->group(function() {
        Route::name('admin.awards.index')->get('/', 'Awards\AwardController@index');
        Route::name('admin.awards.create')->get('/adicionar', 'Awards\AwardController@create');
        Route::name('admin.awards.store')->post('/adicionar', 'Awards\AwardController@store');
        Route::name('admin.awards.edit')->get('/editar/{id}', 'Awards\AwardController@edit');
        Route::name('admin.awards.update')->put('/editar/{id}', 'Awards\AwardController@update');
        Route::name('admin.awards.destroy')->delete('/excluir/{id}', 'Awards\AwardController@destroy');
        Route::name('admin.awards.reorder')->post('/reordenar', 'Awards\AwardController@reorder');

        Route::prefix('categorias')->group(function() {
          Route::name('admin.awards_categories.index')->get('/', 'Awards\AwardCategoryController@index');
          Route::name('admin.awards_categories.create')->get('/adicionar', 'Awards\AwardCategoryController@create');
          Route::name('admin.awards_categories.store')->post('/adicionar', 'Awards\AwardCategoryController@store');
          Route::name('admin.awards_categories.edit')->get('/editar/{id}', 'Awards\AwardCategoryController@edit');
          Route::name('admin.awards_categories.update')->put('/editar/{id}', 'Awards\AwardCategoryController@update');
          Route::name('admin.awards_categories.destroy')->delete('/excluir/{id}', 'Awards\AwardCategoryController@destroy');
          Route::name('admin.awards_categories.reorder')->post('/reordenar', 'Awards\AwardCategoryController@reorder');
        });
      });

      // Units
      Route::prefix('unidades')->group(function() {
        Route::name('admin.units.index')->get('/', 'Units\UnitController@index');
        Route::name('admin.units.create')->get('/adicionar', 'Units\UnitController@create');
        Route::name('admin.units.store')->post('/adicionar', 'Units\UnitController@store');
        Route::name('admin.units.edit')->get('/editar/{id}', 'Units\UnitController@edit');
        Route::name('admin.units.update')->put('/editar/{id}', 'Units\UnitController@update');
        Route::name('admin.units.destroy')->delete('/excluir/{id}', 'Units\UnitController@destroy');
      });

      // Permissions
      Route::prefix('permissoes')->group(function() {
        Route::name('admin.permissions.index')->get('/', 'Users\PermissionController@index');
        Route::name('admin.permissions.create')->get('/adicionar', 'Users\PermissionController@create');
        Route::name('admin.permissions.store')->post('/adicionar', 'Users\PermissionController@store');
        Route::name('admin.permissions.edit')->get('/editar/{id}', 'Users\PermissionController@edit');
        Route::name('admin.permissions.update')->put('/editar/{id}', 'Users\PermissionController@update');
        Route::name('admin.permissions.destroy')->delete('/excluir/{id}', 'Users\PermissionController@destroy');
      });

      // Roles
      Route::prefix('grupos')->group(function() {
        Route::name('admin.roles.index')->get('/', 'Users\RoleController@index');
        Route::name('admin.roles.create')->get('/adicionar', 'Users\RoleController@create');
        Route::name('admin.roles.store')->post('/adicionar', 'Users\RoleController@store');
        Route::name('admin.roles.edit')->get('/editar/{id}', 'Users\RoleController@edit');
        Route::name('admin.roles.update')->put('/editar/{id}', 'Users\RoleController@update');
      });

      // Users
      Route::prefix('usuarios')->group(function() {
        Route::name('admin.users.index')->get('/', 'Users\UserController@index');
        Route::name('admin.users.create')->get('/adicionar', 'Users\UserController@create');
        Route::name('admin.users.store')->post('/adicionar', 'Users\UserController@store');
        Route::name('admin.users.edit')->get('/editar/{id}', 'Users\UserController@edit');
        Route::name('admin.users.update')->put('/editar/{id}', 'Users\UserController@update');
        Route::name('admin.users.activate')->get('/ativar/{id}', 'Users\UserController@activate');
        Route::name('admin.users.deactivate')->get('/desativar/{id}', 'Users\UserController@deactivate');
        Route::name('admin.users.sendEmail')->get('/reenviar/{id}', 'Users\UserController@sendEmail');
      });

      // Audit
      Route::prefix('auditoria')->group(function() {
        Route::name('admin.audits.index')->get('/', 'Audits\AuditController@index');
        Route::name('admin.audits.show')->get('/{id}', 'Audits\AuditController@show');
      });

    });
  });
});
