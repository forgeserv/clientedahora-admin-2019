<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Api Routes
 */
Route::prefix('api')->namespace('Api')->group(function()
{
  Route::prefix('v1')->group(function()
  {
    // Auth
    Route::prefix('auth')->group(function()
    {
      Route::name('auth.login.authenticate')->post('/', 'Auth\AuthController@authenticate');
      Route::name('auth.login.logout')->get('/logout', 'Auth\AuthController@logout');
      Route::name('auth.login.refreshToken')->post('/refresh-token', 'Auth\AuthController@refreshToken');
    });

    // Clients
    Route::prefix('clients')->group(function()
    {
      Route::name('api.clients.avatar')->post('/avatar', 'Clients\ClientController@avatar');
      Route::name('api.clients.store')->post('/', 'Clients\ClientController@store');
      Route::name('api.clients.show')->get('/me', 'Clients\ClientController@show');
      Route::name('api.clients.update')->post('/update', 'Clients\ClientController@update');
      Route::name('api.clients.updatePassword')->post('/password', 'Clients\ClientController@updatePassword');
      Route::name('api.clients.awards')->get('/awards', 'Clients\ClientController@awards');
      Route::name('api.clients.awardsHistory')->get('/awards/histories', 'Clients\ClientController@awardsHistory');
    });

    // Promotions
    Route::prefix('promotions')->group(function()
    {
      Route::name('api.promotions.index')->get('/', 'Promotions\PromotionController@index');
      Route::name('api.promotions.featured')->get('/featured', 'Promotions\PromotionController@featured');
      Route::name('api.promotions.show')->get('/me', 'Promotions\PromotionController@show');
      Route::name('api.promotions.categories')->get('/categories', 'Promotions\PromotionController@categories');
      Route::name('api.promotions.toFavor')->get('/tofavor/{id}', 'Promotions\PromotionController@toFavor');
      Route::name('api.promotions.promotionsByCategory')->get('/categories/{id}', 'Promotions\PromotionController@promotionsByCategory');
    });

    // Awards
    Route::prefix('awards')->group(function()
    {
      Route::name('api.awards.index')->get('/', 'Awards\AwardController@index');
      Route::name('api.awards.featured')->get('/featured', 'Awards\AwardController@featured');
      Route::name('api.awards.show')->get('/show/{id}', 'Awards\AwardController@show');
      Route::name('api.awards.redeem')->post('/redeem/{id}', 'Awards\AwardController@redeem');
      Route::name('api.awards.categories')->get('/categories', 'Awards\AwardController@categories');
      Route::name('api.awards.awardsByCategory')->get('/categories/{id}', 'Awards\AwardController@awardsByCategory');
    });
  });
});
