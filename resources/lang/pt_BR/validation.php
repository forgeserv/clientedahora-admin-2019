<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Validation Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines contain the default error messages used by
  | the validator class. Some of these rules have multiple versions such
  | as the size rules. Feel free to tweak each of these messages here.
  |
  */

  'accepted'             => ':attribute precisa ser aceito.',
  'active_url'           => ':attribute infelizmente não é uma URL válida.',
  'after'                => ':attribute precisa ser uma data depois de :date.',
  'after_or_equal'       => ':attribute precisa ser uma data posterior ou igual a :date.',
  'alpha'                => 'Apenas letras.',
  'alpha_dash'           => 'Apenas letras, números e traços.',
  'alpha_num'            => 'Apenas letras e números.',
  'array'                => ':attribute precisa ser um array.',
  'before'               => ':attribute precisa ser uma data antes de :date.',
  'before_or_equal'      => ':attribute precisa ser uma data anterior ou igual a :date.',
  'between'              => [
      'numeric' => 'Ops, :attribute deve estar entre :min e :max.',
      'file'    => 'Ops, :attribute deve estar entre :min e :max kilobytes.',
      'string'  => 'Ops, :attribute deve estar entre :min e :max caracteres.',
      'array'   => 'Ops, :attribute precisa ter entre :min e :max itens.',
  ],
  'boolean'              => ':attribute precisa ser verdadeiro ou falso.',
  'confirmed'            => 'Campos não conferem.',
  'date'                 => 'Data inválida.',
  'date_format'          => 'Formato inválido.',
  'different'            => ':attribute e :other devem ser diferentes.',
  'digits'               => ':attribute precisa ter :digits dígitos.',
  'digits_between'       => ':attribute precisa ter entre :min e :max dígitos.',
  'dimensions'           => ':attribute tem dimensões de imagem inválidas.',
  'distinct'             => ':attribute tem um valor duplicado.',
  'email'                => ':attribute precisa ser um endereço de e-mail válido.',
  'exists'               => ':attribute selecionado é inválido.',
  'file'                 => 'Precisa ser um arquivo.',
  'filled'               => ':attribute é um campo requerido.',
  'image'                => 'Precisa ser uma imagem.',
  'in'                   => ':attribute é inválido.',
  'in_array'             => ':attribute não existe em :other.',
  'integer'              => ':attribute precisa ser um inteiro.',
  'ip'                   => ':attribute precisa ser um endereço IP válido.',
  'json'                 => ':attribute precisa ser um JSON válido.',
  'max'                  => [
      'numeric' => 'Não pode ser maior que :max.',
      'file'    => 'Não pode ter mais que :max kilobytes.',
      'string'  => 'Não pode ter mais que :max caracteres.',
      'array'   => 'Não pode ter mais que :max itens.',
  ],
  'mimes'                => ':attribute precisa ser um arquivo do tipo: :values.',
  'mimetypes'            => ':attribute precisa ser um arquivo do tipo: :values.',
  'min'                  => [
      'numeric' => 'Precisa ser no mínimo :min.',
      'file'    => 'Precisa ter no mínimo :min kilobytes.',
      'string'  => 'Mínimo de :min caracteres.',
      'array'   => 'Precisa ter no mínimo :min itens.',
  ],
  'not_in'               => 'O :attribute selecionado é inválido.',
  'numeric'              => 'Precisa ser um número.',
  'present'              => 'O campo :attribute precisa ser presente.',
  'regex'                => 'O formato de :attribute é inválido.',
  'required'             => 'Campo obrigatório.',
  'required_if'          => 'Campo obrigatório.',
  'required_unless'      => 'Campo obrigatório.',
  'required_with'        => 'Campo obrigatório.',
  'required_with_all'    => 'Campo obrigatório.',
  'required_without'     => 'Campo obrigatório.',
  'required_without_all' => 'Campo obrigatório.',
  'same'                 => ':attribute e :other devem ser iguais.',
  'size'                 => [
      'numeric' => 'Ops, :attribute precisa ser :size.',
      'file'    => 'Ops, :attribute precisa ter :size kilobytes.',
      'string'  => 'Ops, :attribute precisa ter :size caracteres.',
      'array'   => 'Ops, :attribute deve conter :size itens.',
  ],
  'string'                  => 'Ops, precisa ser uma string.',
  'timezone'                => 'Ops, precisa ser uma timezone válida.',
  'unique'                  => 'Ops, já está em uso.',
  'uploaded'                => 'Ops, falhou ao ser enviado.',
  'url'                     => 'Url inválida.',
  'phone'                   => 'Telefone inválido.',

  'postalcode_in_range'     => 'Não pode estar em uma faixa de CEP existente',
  'postalcode_out_range'    => 'Não pode englobar uma faixa de CEP existente',
  'after_or_equal_numeric'  => 'Precisa ser maior ou igual a :compare.',
  'before_or_equal_numeric' => 'Precisa ser menor ou igual a :compare.',

  /*
  |--------------------------------------------------------------------------
  | Custom Validation Language Lines
  |--------------------------------------------------------------------------
  |
  | Here you may specify custom validation messages for attributes using the
  | convention "attribute.rule" to name the lines. This makes it quick to
  | specify a specific custom language line for a given attribute rule.
  |
  */

  'custom' => [
      'attribute-name' => [
          'rule-name' => 'custom-message',
      ],
  ],

  /*
  |--------------------------------------------------------------------------
  | Custom Validation attributes
  |--------------------------------------------------------------------------
  |
  | The following language lines are used to swap attribute place-holders
  | with something more reader friendly such as E-Mail Address instead
  | of "email". This simply helps us make messages a little cleaner.
  |
  */

  'attributes' => [],

];
