@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('audits.index') }}
    </div>
    <div class="row">
      <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                {!! Form::open(['route' => 'admin.audits.index', 'method' => 'GET', 'novalidate', 'role' => 'form']) !!}
                  <div class="row">
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      {!! Form::text('data', Request::get('data'), ['class' => 'form-control form-control-lg js-datepicker', 'placeholder' => 'Data']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      {!! Form::select('usuario', $users, Request::get('usuario'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Usuário']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      {!! Form::select('tipo', config('panel.audit.types'), Request::get('tipo'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Tipo']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      {!! Form::select('acao', config('panel.audit.actions'), Request::get('acao'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Ação']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="button" class="btn btn-danger btn-lg btn-block"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Auditoria</h4>
            <span class="legend">Total: {{ $results->total() }}</span>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> Usuário </th>
                  <th> Tipo </th>
                  <th> Ação </th>
                  <th> Horário </th>
                  <th> Ações </th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  <tr>
                    <td>
                      @if($result->user->photo)
                       <img src="{{ $result->user->photo }}" alt="{{ $result->user->full_name }}">
                      @endif
                      {{ $result->user->full_name }}
                     </td>
                    <td> {{ $result->type_name }} </td>
                    <td> {{ $result->action_name }} </td>
                    <td> {{ $result->created_at->format('d/m/Y H:i') }} </td>
                    <td>
                      <a href="{{ route('admin.audits.show', ['id' => $result->id]) }}" class="action-icon -success" data-toggle="tooltip" data-placement="bottom" title="Visualizar" data-original-title="Visualizar"><i class="icon-eye"></i></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @include('admin.partials.pagination')
        </div>
      </div>
    </div>
  </div>
@endsection
