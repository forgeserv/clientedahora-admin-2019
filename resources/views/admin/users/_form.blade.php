<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center sac">
      <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-key"></i> Acesso</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-md-12 col-lg-12 col-sm-12">
          {!! Form::label('role', 'Grupo', []) !!}
          {!! Form::select('role', $roles, isset($userRole) ? $userRole : old('role'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Grupo']) !!}
          @if($errors->has('role'))
            <span class="help-block">{{ $errors->first('role') }}</span>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 grid-margin">
  <div class="card review-card bottom">
    <div class="card-header header-sm d-flex justify-content-between align-items-center">
      <h4 class="card-title"> <i class="link-icon mdi mdi-account"></i> Dados pessoais</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('first_name', 'Nome', []) !!}
          {!! Form::text('first_name', old('first_name'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('first_name'))
            <span class="help-block">{{ $errors->first('first_name') }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('last_name', 'Sobrenome', []) !!}
          {!! Form::text('last_name', old('last_name'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('last_name'))
            <span class="help-block">{{ $errors->first('last_name') }}</span>
          @endif
        </div>
        @if(isset($result))
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('password', 'Senha', []) !!}
          {!! Form::password('password', ['class' => 'form-control form-control-lg']) !!}

          @if($errors->has('password'))
          <span class="help-block">{{ $errors->first('password') }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('password_confirmation', 'Confirmar senha', []) !!}
          {!! Form::password('password_confirmation', ['class' => 'form-control form-control-lg']) !!}

          @if($errors->has('password_confirmation'))
          <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
          @endif
        </div>
        @endif
      </div>
    </div>
  </div>

  <div class="card review-card">
    <div class="card-header header-sm d-flex justify-content-between align-items-center">
      <h4 class="card-title"> <i class="link-icon mdi mdi-email-outline"></i> Contatos</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('email', 'E-mail', []) !!}
          {!! Form::email('email', old('email'), ['class' => 'form-control form-control-lg']) !!}
          @if($errors->has('email'))
            <span class="help-block">{{ $errors->first('email') }}</span>
          @endif
        </div>
        <div class="form-group col-md-6 col-lg-6 col-sm-12">
          {!! Form::label('phone', 'Telefone', []) !!}
          {!! Form::text('phone', old('phone'), ['class' => 'form-control form-control-lg js-mask-phone']) !!}
          @if($errors->has('phone'))
            <span class="help-block">{{ $errors->first('phone') }}</span>
          @endif
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12">
          <div class="icheck-flat">
            {!! Form::hidden('receive_messages', false) !!}
            {!! Form::checkbox('receive_messages', true, isset($result) ? $result->receive_messages : false, ['class' => '', 'id' => 'receive_messages']) !!}
            {!! Form::label('receive_messages', 'Receber mensagens?', []) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
