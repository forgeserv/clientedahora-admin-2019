@component('mail::message')
# Olá, {{ $client->full_name }}

Em nome de toda a equipe da {{ config('app.name') }} gostaríamos de te dar as boas-vindas!

Sua senha de acesso para o aplicativo é: <b>clientedahora</b>

O próximo passo é ativar sua conta.

@component('mail::button', ['url' => $url, 'color' => 'green'])
  Clique aqui para ativar sua conta!
@endcomponent

Atenciosamente,<br>
{{ config('app.name') }}
@endcomponent
