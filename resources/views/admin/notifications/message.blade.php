@component('mail::message')
# Olá, {{ $user->full_name }}

Você acaba de receber uma nova mensagem do site <b>{{ config('app.name') }}</b>

<b>Remetente</b> - {{ $contact->name }}

<b>E-mail</b> - {{ $contact->email }}

<b>Assunto</b> - {{ $contact->subject }}

<b>Mensagem</b> - {{ $contact->body }}

<b>Enviado no dia</b> - {{ $contact->created_at->formatLocalized('%d %B %Y') }}

@component('mail::button', ['url' => $url, 'color' => 'green'])
  Visualizar detalhes
@endcomponent

Atenciosamente,<br>
{{ config('app.name') }}
@endcomponent
