@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('clients.index') }}
    </div>
    <div class="row">
      <div class="col-12 grid-margin">
        <div class="card card-statistics">
          <div class="row">
            @can('add_clients')
              <div class="card-col col-sm-12 col-xl-12 col-lg-12 col-md-12 col-12">
                <div class="card-body">
                  <a href="{{ route('admin.clients.create') }}" class="hover-me" title="Adicionar"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-account-plus text-success mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Adicionar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Cliente</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
          </div>
        </div>
      </div>
      <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                {!! Form::open(['route' => 'admin.clients.index', 'method' => 'GET', 'novalidate', 'role' => 'form']) !!}
                  <div class="row">
                    <div class="form-group no-margin col-md-8 col-lg-8 col-sm-12">
                      {!! Form::text('nome', Request::get('nome'), ['class' => 'form-control form-control-lg', 'placeholder' => 'Nome']) !!}
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="button" class="btn btn-danger btn-lg btn-block"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="form-group no-margin col-md-2 col-lg-2 col-sm-12">
                      <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Clientes</h4>
            <span class="legend">Total: {{ $results->total() }}</span>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> Nome </th>
                  <th> E-mail </th>
                  <th> Pontos </th>
                  <th> Gastos </th>
                  <th> Status </th>
                  <th> Ações </th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  <tr>
                    <td class="py-1">
                      <img src="{{ $result->photo }}" alt="{{ $result->full_name }}">
                      {{ $result->full_name }}
                    </td>
                    <td> {{ $result->email }} </td>
                    <td> {!! $result->salt_points !!} </td>
                    <td> {{ money($result->cash_total, 'BRL') }} </td>
                    <td> {!! isActive($result->active) !!} </td>
                    <td>
                      @can('edit_clients')
                        <a href="{{ route('admin.clients.show', ['id' => $result->id]) }}" class="action-icon -warning" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Visualizar"><i class="icon-eye"></i></a>
                      @endcan

                      @can('edit_clients')
                        <a href="{{ route('admin.clients.edit', ['id' => $result->id]) }}" class="action-icon -warning" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Editar"><i class="icon-pencil"></i></a>
                      @endcan

                       @can('delete_clients')
                        <a href="#" data-link="{{ route('admin.clients.destroy', ['id' => $result->id]) }}" class="action-icon js-confirm-delete-release -danger" data-toggle="tooltip" data-placement="bottom" data-title="{{ $result->full_name }}" title="" data-original-title="Excluir"><i class="icon-trash"></i></a>
                      @endcan
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @include('admin.partials.pagination')
        </div>
      </div>
    </div>
  </div>
@endsection
