@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('accounts.show') }}
    </div>
    <div class="row">
      <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
          <div class="col-12">
            <div class="card">
              <div class="card-body reset-padding margin-1">
                <div class="card-action-title">
                  <h3> Meu Perfil
                  </h3>
                </div>
                <div class="card-actions">
                  <div class="margin-btn">
                    <a href="{{ route('admin.dashboard.index') }}" class="btn btn-danger btn-lg" title="Voltar"><i class="fa fa-angle-left"></i>Voltar</a>
                  </div>
                  <div class="">
                    <a href="{{ route('admin.accounts.edit', ['id' => $result->id]) }}" class="btn btn-warning btn-lg"><i class="fa fa-pencil"></i>Editar</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistics social-card card-default">
          <div class="card-header header-sm">
            <div class="d-flex align-items-center">
              <div class="wrapper d-flex align-items-center media-info text-facebook">
                <i class="mdi mdi-account-circle icon-md"></i>
                <h2 class="card-title ml-3">Contato</h2>
              </div>
            </div>
          </div>
          <div class="card-body">
            <img class="d-block img-sm rounded-circle mx-auto mb-2" src="{{ $result->photo }}" alt="{{ $result->name }}">
            <p class="text-center user-name">{{ $result->full_name }}</p>
            <p class="text-center mb-2 comment">{{ $result->email }}</p>
            <small class="d-block mt-4 text-center posted-date">{{ $result->created_at->format('d/m/Y H:i') }}</small>
          </div>
          <div class="card-footer"></div>
        </div>
      </div>
      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics social-card card-default">
          <div class="card-header header-sm">
            <div class="d-flex align-items-center">
              <div class="wrapper d-flex align-items-center media-info text-facebook">
                <i class="mdi mdi-star-outline icon-md"></i>
                <h2 class="card-title ml-3">Atividades recentes</h2>
              </div>
            </div>
          </div>
          <div class="card-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> Usuário </th>
                  <th> Tipo </th>
                  <th> Ação </th>
                  <th> Horário </th>
                  <th> Ações </th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  <tr>
                    <td> {{ $result->user->full_name }} </td>
                    <td> {{ $result->type_name }} </td>
                    <td> {{ $result->action_name }} </td>
                    <td> {{ $result->created_at->format('d/m/Y H:i') }} </td>
                    <td>
                      <a href="{{ route('admin.audits.show', ['id' => $result->id]) }}" class="action-icon -success" data-toggle="tooltip" data-placement="bottom" title="Visualizar" data-original-title="Visualizar"><i class="icon-eye"></i></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @include('admin.partials.pagination')
        </div>
      </div>
    </div>
  </div>
@endsection
