@extends('admin.layouts.public')

@section('content')
<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      {!! Form::open(['class' => 'login100-form validate-form', 'route' => 'admin.session.login', 'novaldiate', 'method' => 'post']) !!}
        <span class="login100-form-title p-b-43">
          Painel Administrativo
        </span>

         <div class="wrap-input100 validate-input">
          {!! Form::select('unit_id', [], old('unit_id'), ['class' => 'input100']) !!}
          <span class="focus-input100"></span>
          <span class="label-input100">Selecione uma unidade</span>
        </div>

        <div class="wrap-input100 validate-input">
          {!! Form::text('email', old('email'), ['class' => 'input100'] ) !!}
          <span class="focus-input100"></span>
          <span class="label-input100">Email</span>
        </div>

        <div class="wrap-input100 validate-input">
          {!! Form::password('password', ['class' => 'input100'] ) !!}
          <span class="focus-input100"></span>
          <span class="label-input100">Senha</span>
        </div>

        <div class="flex-sb-m w-full p-t-3 p-b-32">
          <div class="contact100-form-checkbox">
            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
            <label class="label-checkbox100" for="ckb1">
              Lembre de mim
            </label>
          </div>

          <div>
            <a href="#" class="txt1">
              Esqueceu a senha?
            </a>
          </div>
        </div>

        <div class="container-login100-form-btn">
          {!! Form::button('Acessar', ['class' => 'login100-form-btn', 'type' => 'submit']) !!}
        </div>

      {!! Form::close() !!}

      <div class="login100-more" style="background-image: url('{{ asset('/assets_login/002.jpg') }}');">
      </div>
    </div>
  </div>
</div>
@stop
