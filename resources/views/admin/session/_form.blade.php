  {!! Form::open(['method' => 'post', 'class' => 'form -boxed -white', 'style' => 'padding: 0 !important;', 'novalidate', 'route' => 'admin.session.login']) !!}
  <div class="row">
    <div class="form-group col-md-6">
      {!! Form::label('email','E-mail', ['class' => 'label']) !!}
      {!! Form::text('email', old('email'), ['class' => 'form-control input']) !!}
      @if($errors->has('email'))
        <span class="help-block">{{ $errors->first('email') }}</span>
      @endif
    </div>
    <div class="form-group col-md-6">
      {!! Form::label('password','Senha', ['class' => 'label']) !!}
      {!! Form::password('password', ['class' => 'form-control input']) !!}
      @if($errors->has('password'))
        <span class="help-block">{{ $errors->first('password') }}</span>
      @endif
    </div>
    <div class="form-actions col-md-4 pull-right">
      <button class="btn -inline -primary-b -block">Acessar</button>
    </div>
  </div>
{!! Form::close() !!}
