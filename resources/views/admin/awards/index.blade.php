@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('awards.index') }}
    </div>
    <div class="row">
      <div class="col-12 grid-margin">
        <div class="card card-statistics">
          <div class="row">
            @can('view_awards_submodules')
              <div class="card-col col-xl-6 col-lg-6 col-md-6 col-12">
                <div class="card-body">
                  <a href="{{ route('admin.awards_categories.index') }}" class="hover-me" title="Lista"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-tag text-info mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Lista</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Categorias</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
            @can('add_awards')
              <div class="card-col col-xl-6 col-lg-6 col-md-6 col-12">
                <div class="card-body">
                  <a href="{{ route('admin.awards.create') }}" class="hover-me" title="Adicionar"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-plus text-success mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Adicionar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Prêmio</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Prêmios</h4>
            <hr>
            <div class="row" id="profile-list-left" data-url="{{ route('admin.awards.reorder') }}">
              @foreach($results as $result)
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12" data-id="{{ $result->id }}">
                  <div class="card review-card m-bottom">
                    <div class="card-header header-sm d-flex justify-content-between align-items-center">
                      <h4 class="card-title">{{ $result->name }}</h4>
                    </div>
                    <div class="card-body">
                      @if($result->thumb)
                        <div class="release">
                          <img src="{{ $result->thumb  }}" alt="{{ $result->name }}"> </a>
                        </div>
                      @endif
                    </div>
                    <div class="card-footer -releases">
                      <div class="cards-actions text-right">

                        @can('edit_awards')
                        <a href="{{ route('admin.awards.edit', ['id' => $result->id]) }}" class="action-icon -warning" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Editar"><i class="icon-pencil"></i></a>
                        @endcan

                        @can('delete_awards')
                        <a href="#" data-link="{{ route('admin.awards.destroy', ['id' => $result->id]) }}" class="action-icon js-confirm-delete-release -danger" data-toggle="tooltip" data-placement="bottom" data-title="{{ $result->name }}" title="" data-original-title="Excluir"><i class="icon-trash"></i></a>
                        @endcan
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
