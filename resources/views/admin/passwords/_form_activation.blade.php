{!! Form::open(['class' => 'form -white', 'route' => 'admin.activation.processCreate', 'method' => 'POST', 'novalidate', 'role' => 'form']) !!}
  <div class="row">
    <div class="form-group col-md-6">
      {!! Form::label('password', 'Senha', ['class' => 'label sr-only']) !!}
      {!! Form::password('password', ['class' => 'form-control input', 'placeholder' => 'Senha']) !!}
      @if($errors->has('password'))
        <span class="help-block">{{ $errors->first('password') }}</span>
      @endif
    </div>
    <div class="form-group col-md-6">
      {!! Form::label('password_confirmation', 'Confirmar a senha', ['class' => 'label sr-only']) !!}
      {!! Form::password('password_confirmation', ['class' => 'form-control input', 'placeholder' => 'Confirmar a senha']) !!}
      @if($errors->has('password_confirmation'))
        <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
      @endif
    </div>
    <div class="form-actions col-md-4 pull-right">
      {!! Form::button('Registrar', ['class' => 'btn -inline -primary-b -block', 'type' => 'submit']) !!}
    </div>
  </div>
{!! Form::close() !!}
