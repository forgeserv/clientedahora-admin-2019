@extends('admin.layouts.public')

@section('content')
@include('flash::message')

<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      {!! Form::open(['class' => 'login100-form', 'route' => 'admin.activation.processCreate', 'novaldiate', 'method' => 'post']) !!}
        <span class="login100-form-title p-b-43">
          Painel Administrativo
        </span>

        <div class="wrap-input100 validate-input">
          {!! Form::password('password', ['class' => 'input100'] ) !!}
          <span class="focus-input100"></span>
          <span class="label-input100">Senha</span>
           @if($errors->has('password'))
            <span class="help-block">{{ $errors->first('password') }}</span>
          @endif
        </div>

        <div class="wrap-input100 validate-input">
          {!! Form::password('password_confirmation', ['class' => 'input100'] ) !!}
          <span class="focus-input100"></span>
          <span class="label-input100">Confirmar senha</span>
           @if($errors->has('password_confirmation'))
            <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
          @endif
        </div>

        <div class="container-login100-form-btn">
          {!! Form::button('Nova senha', ['class' => 'login100-form-btn', 'type' => 'submit']) !!}
        </div>

      {!! Form::close() !!}

      <div class="login100-more" style="background-image: url('{{ asset('/assets_login/002.jpg') }}');">
      </div>
    </div>
  </div>
</div>
@stop
