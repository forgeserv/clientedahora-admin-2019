@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    <div class="row">
      {{ Breadcrumbs::render('permissions.index') }}
    </div>
    <div class="row">
      <div class="col-12 grid-margin">
        <div class="card card-statistics">
          <div class="row">
            @can('view_users')
              <div class="card-col col-xl-6 col-lg-6 col-md-6 col-6">
                <div class="card-body">
                  <a href="{{ route('admin.users.index') }}" class="hover-me" title="Voltar"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-arrow-left text-danger mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Voltar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Usuários</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
            @can('add_permissions')
              <div class="card-col col-xl-6 col-lg-6 col-md-6 col-6">
                <div class="card-body">
                  <a href="{{ route('admin.permissions.create') }}" class="hover-me" title="Adicionar"></a>
                  <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                    <i class="mdi mdi-account-plus text-success mr-0 mr-sm-4 icon-lg"></i>
                    <div class="wrapper text-center text-sm-left">
                      <p class="card-text mb-0">Adicionar</p>
                      <div class="fluid-container">
                        <h3 class="mb-0 font-weight-medium">Permissão</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endcan
          </div>
        </div>
      </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Permissões</h4>
            <span class="legend">Total: {{ $results->total() }}</span>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> Nome </th>
                  <th> Detalhes </th>
                  <th> Ações </th>
                </tr>
              </thead>
              <tbody>
                @foreach($results as $result)
                  <tr>
                    <td> {{ $result->name }} </td>
                    <td> {!! $result->details !!} </td>
                    <td>
                      @can('edit_permissions')
                        <a href="{{ route('admin.permissions.edit', ['id' => $result->id]) }}" class="action-icon -warning" data-toggle="tooltip" data-placement="bottom" title="Editar" data-original-title="Editar"><i class="icon-pencil"></i></a>
                      @endcan
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @include('admin.partials.pagination')
        </div>
      </div>
    </div>
  </div>
@endsection
