@extends('admin.layouts.default')

@section('content')
  <div class="content-wrapper">
    @include('flash::message')
    {!! Form::model($result, ['route' => ['admin.units.update', 'id' => $result->id], 'method' => 'PUT', 'novalidate', 'role' => 'form', 'files' => true]) !!}
      <div class="row">
        {{ Breadcrumbs::render('units.edit', $result) }}
        <div class="col-md-12 d-flex align-items-stretch grid-margin">
          <div class="row flex-grow">
            <div class="col-12">
              <div class="card">
                <div class="card-body reset-padding margin-1">
                  <div class="card-action-title">
                    <h3> Editar Unidade
                      <small class="text-muted"> Preencha os campos abaixo. </small>
                    </h3>
                  </div>
                  <div class="card-actions">
                    <div class="margin-btn">
                      <a href="{{ route('admin.units.index') }}" class="btn btn-danger btn-lg" title="Voltar"><i class="fa fa-angle-left"></i>Voltar</a>
                    </div>
                    <div class="">
                      <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save"></i>Salvar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       @include('admin.units._form')
      </div>
    {!! Form::close() !!}
  </div>
@endsection
