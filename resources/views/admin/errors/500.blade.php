@extends('admin.layouts.error')

@section('content')
<div class="content-wrapper d-flex align-items-center text-center error-page bg-danger">
  <div class="row flex-grow">
    <div class="col-lg-7 mx-auto text-white">
      <div class="row align-items-center d-flex flex-row">
        <div class="col-lg-6 text-lg-right pr-lg-4">
          <h1 class="display-1 mb-0">500</h1>
        </div>
        <div class="col-lg-6 error-page-divider text-lg-left pl-lg-4">
          <h2>Desculpe!</h2>
          <h3 class="font-weight-light">Serviço indisponível, nossos técnicos já foram informados. Tente novamente mais tarde.</h3>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12 text-center mt-xl-2">
          <a class="text-white font-weight-medium" href="{{ route('admin.dashboard.index') }}" title="Volte aqui">Volte aqui</a>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12 mt-xl-2">
          <p class="text-white font-weight-medium text-center">Copyright © 2019 All rights reserved, {{ config('app.name') }}</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
