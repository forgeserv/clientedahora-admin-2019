@if (count($breadcrumbs))
  <div class="col-md-12 col-lg-12 col-sm-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-primary">
        @foreach ($breadcrumbs as $breadcrumb)
          @if ($breadcrumb->url && !$loop->last)
            <li class="breadcrumb-item">
              <a href="{{ $breadcrumb->url }}" title="{{ $breadcrumb->title }}">{{ $breadcrumb->title }}</a>
            </li>
          @else
            <li class="breadcrumb-item active">
              <a href="#" title="{{ $breadcrumb->title }}">{{ $breadcrumb->title }}</a>
            </li>
          @endif
        @endforeach
      </ol>
    </nav>
  </div>
@endif
