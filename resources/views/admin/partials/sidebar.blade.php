<div class="sidebar-menu">
  <nav class="nav">
    <div class="nav-item">
      <a href="index.html" class="nav-link">
        <i class="menu-icons mdi mdi-signal-cellular-outline"></i><span class="menu-title">Dashboard</span>
      </a>
    </div>
    <div class="nav-item nav-category">Comercial</div>
    <div class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#dashboard-dropdown" aria-expanded="false" aria-controls="dashboard-dropdown">
        <i class="menu-icons mdi mdi-locker-multiple"></i>
        <span class="menu-title">Pedidos</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="dashboard-dropdown">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="blank-page.html">Relatório diário</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="blank-page.html">Todos os pedidos</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="nav-item">
      <a href="#" class="nav-link">
        <i class="menu-icons mdi mdi-trophy-outline"></i><span class="menu-title">Prêmios</span>
      </a>
    </div>
    <div class="nav-item">
      <a href="#" class="nav-link">
        <i class="menu-icons mdi mdi-gift"></i><span class="menu-title">Promoções</span>
      </a>
    </div>
    <div class="nav-item">
      <a href="#" class="nav-link">
        <i class="menu-icons mdi mdi-star-outline"></i><span class="menu-title">Destaques</span>
      </a>
    </div>
    <div class="nav-item nav-category">CRM</div>
    <div class="nav-item">
      <a href="blank-page.html" class="nav-link">
        <i class="menu-icons mdi mdi-account-circle-outline"></i><span class="menu-title">Usuários</span>
      </a>
    </div>
    <div class="nav-item">
      <a href="blank-page.html" class="nav-link">
        <i class="menu-icons mdi mdi-briefcase-account-outline"></i><span class="menu-title">Perfil dos clientes</span>
      </a>
    </div>
    <div class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#resources-dropdown" aria-expanded="false" aria-controls="dashboard-dropdown">
        <i class="menu-icons mdi mdi-map-marker"></i>
        <span class="menu-title">Unidades</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="resources-dropdown">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="blank-page.html">Criar nova unidade</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="blank-page.html">Todas as unidades</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="nav-item">
      <a href="#" class="nav-link">
        <i class="menu-icons mdi mdi-alert-circle-outline"></i><span class="menu-title">Notificações</span>
      </a>
    </div>
    <div class="nav-item nav-category">Configurações</div>
    <div class="nav-item">
      <a href="{{ route('admin.roles.index') }}" class="nav-link" title="Grupos">
        <i class="menu-icons mdi mdi-account-multiple-outline"></i>
        <span class="menu-title">Grupos</span>
      </a>
    </div>
    <div class="nav-item">
      <a href="blank-page.html" class="nav-link">
        <i class="menu-icons mdi mdi-shield-key-outline"></i><span class="menu-title">Permissões</span>
      </a>
    </div>
    <div class="nav-item">
      <a href="blank-page.html" class="nav-link">
        <i class="menu-icons mdi mdi-eye-outline"></i><span class="menu-title">Auditoria</span>
      </a>
    </div>
  </nav>
  <div class="account-bar">
    <div class="bar-actions">
      <a href="blank-page.html" class="action-link"><i class="mdi mdi-flask-outline"></i></a>
      <div class="profile-wrapper">
        <img class="img-sm rounded-circle" src="{{ auth()->user()->photo }}" alt="profile images">
        <div class="dot-indicator bg-success"></div>
      </div>
      <a href="blank-page.html" class="action-link"><i class="mdi mdi-bell-outline"></i></a>
    </div>
    <button class="btn btn-block btn-primary">Editar</button>
  </div>
  <div class="sidebar-footer">
    <p class="mb-0">© CRM Dashboard Ltd. 2019</p>
    <small class="text-muted d-block mt-2">Copyright © 2019 Bootstrapdash. All rights reserved.</small>
  </div>
</div>
