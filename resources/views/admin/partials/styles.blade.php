<!-- plugins:css -->
<link rel="stylesheet" href="{{ asset('/assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/vendors/iconfonts/puse-icons-feather/feather.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/vendors/css/vendor.bundle.base.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/vendors/css/vendor.bundle.addons.css') }}">

<link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/icheck/skins/all.css') }}">
<link rel="stylesheet" href="{{ asset('assets/js-duallistbox/bootstrap-duallistbox.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
<!-- endinject -->
<!-- plugin css for this page -->
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="{{ asset('/assets/css/shared/style.css') }}">
<!-- endinject -->
<!-- Layout styles -->
<link rel="stylesheet" href="{{ asset('/assets/css/demo_2/style.css') }}">

<!-- Custom -->
<link rel="stylesheet" href="{{ asset('/assets/css/custom.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/demo_1/style.css') }}">

<link rel="stylesheet" href="{{ asset('/assets/css/css_dropify/dropzone.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/vendors/summernote/dist/summernote-bs4.css') }}">
