<div class="card review-card bottom">
  <div class="card-header header-sm d-flex justify-content-between align-items-center blue">
    <h4 class="card-title"> <i class="icon-sm link-icon mdi mdi-code-tags"></i> Otimização para buscadores</h4>
    {!! Form::hidden('seo[id]', (isset($result->seo)) ? $result->seo->id : null) !!}
  </div>
  <div class="card-body">
    <div class="row">
      <div class="form-group col-md-12 col-lg-12 col-sm-12">
        {!! Form::label('seo[meta_title]', 'Título da página', []) !!}
        {!! Form::text('seo[meta_title]', (isset($result->seo)) ? $result->seo->meta_title : old('seo.meta_title'), ['class' => 'form-control form-control-lg']) !!}
        @if($errors->has('seo.meta_title'))
          <span class="help-block">{{ $errors->first('seo.meta_title') }}</span>
        @endif
      </div>
      <div class="form-group col-md-12 col-lg-12 col-sm-12">
        {!! Form::label('seo[meta_description]', 'Descrição da página', []) !!}
        {!! Form::text('seo[meta_description]', old('seo[meta_description]'), ['class' => 'form-control form-control-lg']) !!}
        @if($errors->has('seo.meta_description'))
          <span class="help-block">{{ $errors->first('seo.meta_description') }}</span>
        @endif
      </div>
      <div class="form-group col-sm-12 col-lg-12 col-md-12">
        {!! Form::label('seo[image_facebook]', 'Capa facebook (1200x630)', ['']) !!}
        {!! Form::file('seo[image_facebook]', ['class' => 'file-upload-default']) !!}
        <div class="input-group col-xs-12">
          <input type="text" class="form-control file-upload-info" disabled="" placeholder="">
          <span class="input-group-append">
            <button class="file-upload-browse btn btn-default" type="button">Selecionar</button>
          </span>
        </div>
        @if($errors->has('seo.image_facebook'))
          <span class="help-block">{{ $errors->first('seo.image_facebook') }}</span>
        @endif
      </div>
    </div>
  </div>
</div>
