<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon.ico') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.ico') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon.ico') }}">
<link rel="mask-icon" href="{{ asset('/assets/favicons/safari-pinned-tab.svg') }}" color="#f4c42a">
<link rel="manifest" href="{{ asset('/assets/favicons/manifest.json') }}">
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
<meta name="msapplication-TileColor" content="#f4c42a">
<meta name="msapplication-config" content="{{ asset('/assets/favicons/browserconfig.xml') }}">
<meta name="theme-color" content="#f4c42a">
