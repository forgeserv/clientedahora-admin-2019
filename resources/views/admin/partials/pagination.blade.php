<div class="card-footer">
  <div class="col-lg-12">
    <nav class="pull-right">
      <ul class="pagination rounded">
        {!! $results->render() !!}
      </ul>
    </nav>
  </div>
</div>
