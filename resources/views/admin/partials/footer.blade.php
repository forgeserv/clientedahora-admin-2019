<footer class="footer">
  <div class="container clearfix">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
      <a href="{{ route('home.index') }}" target="_blank" title="{{ config('app.name') }} - Todos os direitos reservados.">{{ config('app.name') }}</a>. Todos os direitos reservados.</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Criado e mantido por {{ config('app.name') }}
    </span>
  </div>
</footer>
