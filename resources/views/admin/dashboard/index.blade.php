@extends('admin.layouts.default')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-12 grid-margin d-none d-lg-block">
      <div class="intro-banner">
        <div class="banner-image">
          <img src="{{ asset('/assets/images/dashboard/banner_img.png') }}" alt="Bem-vindo">
        </div>
        <div class="content-area">
          <h3 class="mb-0">Bem-vindo, {{ auth()->user()->full_name }}</h3>
          <p class="mb-0">Este é o seu painel administrativo, onde você vai gerenciar todo o conteúdo do seu site e seus clientes.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-lg-12 grid-margin">
      <div class="card">
        <div class="card-header header-sm d-flex justify-content-between align-items-center">
          <h4 class="card-title">Total de pedidos</h4>
        </div>
        <div class="card-body no-gutter">
          <div class="d-flex align-items-center border-bottom py-3 px-4">
            <img class="rounded-circle img-sm d-none d-lg-block" src="../../../assets/images/file-icons/icon-3.svg" alt="icon">
            <div class="d-flex align-items-end">
              <h6 class="font-weight-medium mb-0 ml-0 ml-md-3">Balanço geral</h6>
            </div>
            <h1 class="ml-auto font-weight-medium">R$13,750</h1>
          </div>
          <div class="d-flex align-items-center border-bottom py-3 px-4">
            <div class=" d-flex flex-column">
              <small class="text-muted">Essa semana</small>
              <div class="d-flex align-items-end">
                <h2 class="font-weight-medium mb-0">234</h2>
                <div class="d-flex align-items-center ml-2">
                  <h5 class="font-weight-medium">32.7%</h5>
                  <i class="mdi mdi mdi-arrow-up mb-2 text-success"></i>
                </div>
              </div>
            </div>
            <div class="w-25 ml-auto"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
              <canvas id="balance-chart-1" height="20" width="182" class="chartjs-render-monitor" style="display: block; width: 182px; height: 20px;"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
   <div class="col-md-4 grid-margin stretch-card">
      <div class="card aligner-wrapper">
        <div class="card-body">
          <div class="absolute left top bottom h-100 v-strock-2 bg-primary"></div>
          <p class="text-muted mb-2">Ganhos em todas unidades</p>
          <div class="d-flex align-items-center">
            <h1 class="font-weight-medium mb-2">R$1,520</h1>
            <h5 class="font-weight-medium text-success ml-2">+20.7%</h5>
          </div>
          <div class="d-flex align-items-center">
            <div class="bg-primary dot-indicator"></div>
            <p class="text-muted mb-0 ml-2">Foram consumidos 403 produtos</p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 grid-margin stretch-card">
      <div class="card aligner-wrapper">
        <div class="card-body">
          <div class="absolute left top bottom h-100 v-strock-2 bg-success"></div>
          <p class="text-muted mb-2">Pontos usados em todas unidades</p>
          <div class="d-flex align-items-center">
            <h1 class="font-weight-medium mb-2">152540</h1>
            <h5 class="font-weight-medium text-success ml-2">+60.7%</h5>
          </div>
          <div class="d-flex align-items-center">
            <div class="bg-success dot-indicator"></div>
            <p class="text-muted mb-0 ml-2">Foram feitos 302 pedidos</p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 grid-margin stretch-card">
      <div class="card aligner-wrapper">
        <div class="card-body">
          <div class="absolute left top bottom h-100 v-strock-2 bg-warning"></div>
          <p class="text-muted mb-2">Pontos atribuitos em todas unidades</p>
          <div class="d-flex align-items-center">
            <h1 class="font-weight-medium mb-2">3152510</h1>
            <h5 class="font-weight-medium text-success ml-2">+10.7%</h5>
          </div>
          <div class="d-flex align-items-center">
            <div class="bg-warning dot-indicator"></div>
            <p class="text-muted mb-0 ml-2">Foram atribuitos a 20 clientes</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
