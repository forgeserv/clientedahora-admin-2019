<div class="card review-card">
  <div class="card-header header-sm d-flex justify-content-between align-items-center">
    <h4 class="card-title"> <i class="link-icon mdi mdi-comment-outline"></i> Mensagens recentes</h4>
  </div>
  <div class="card-body no-gutter">
    @foreach($messages as $message)
      <div class="list-item">
        <a href="{{ route('admin.messages.show', ['id' => $message->id]) }}" class="hover-me" title="{{ $message->name }}"></a>
        <div class="preview-image">
          <img class="img-sm rounded-circle" src="{{ asset('/admin_assets/images/faces/default.png') }}" alt="profile image">
        </div>
        <div class="content">
          <div class="d-flex align-items-center">
            <h6 class="product-name">{{ $message->name }}</h6>
            <small class="time ml-3 d-none d-sm-block">{{ $message->created_at->format('d/m/Y H:i') }}</small>
          </div>
          <div class="d-flex align-items-center">
            <p class="user-name">Mensagem :</p>
            <p class="review-text d-block">{{ str_limit($message->body, 20) }}</p>
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>
