<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#2c4e87">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate() !!}

    @include('admin.partials.favicons')

    @include('admin.partials.styles_login')
  </head>
  <body class="">

    <main class="main" role="main">
      @yield('content')
    </main>

    @include('admin.partials.scripts_login')
  </body>
</html>
