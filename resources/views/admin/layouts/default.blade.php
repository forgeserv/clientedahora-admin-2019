<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="theme-color" content="#2c4e87">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  {!! SEO::generate() !!}
  @include('admin.partials.styles')
  @include('admin.partials.favicons')
</head>
<body>
  <div class="container-scroller">
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
      <div class="main-panel container">
        @yield('content')
        @include('admin.partials.footer')
      </div>
    </div>
  </div>
  @include('admin.partials.scripts')
</body>
</html>
