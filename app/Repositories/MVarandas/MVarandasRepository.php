<?php

namespace App\Repositories\MVarandas;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Cache;

class MVarandasRepository
{
  /**
  * Constants
  */
  CONST API_URL = 'https://www.portalmenew.com.br/public-api/';
  CONST ENV = true;
  CONST USER = 'qualitare';
  CONST PASSWORD = 'qYB_5;^6v5Cj[_*2~W5PXWmcDW';
  CONST ESTABELECIMENTO_ID = '2895';
  CONST CLIENTE_TELEFONE = '999685500';

  /**
  * Variables
  */
  protected $client;

  /**
  * Constructor
  */
  public function __construct()
  {
    // Client Guzzle
    $this->client = new Client();
  }

  /**
  * Get Token
  */
  public function getToken()
  {
    // Headers
    $headers = [
      'Content-Type' => 'application/json'
    ];

    // Body
    $body = [
      "token" => null,
      "requests" => [
        "jsonrpc" => "2.0",
        "method" => "Usuario/login",
        "params" => [
          "usuario" => self::USER,
          "token" => self::PASSWORD,
        ],
        "id" => "3226798"
      ],
    ];

    // Send request
    $response = $this->client->post(self::API_URL, [
      'body' => json_encode($body),
      'headers' => $headers
    ]);

    // Its ok?
    if ($response->getStatusCode() == '200' && !Cache::has('mv_token')) {
      $mvResponse = json_decode($response->getBody()->getContents(), true);
      Cache::store('database')->put('mv_token', $mvResponse["result"], 600);
    } else {
      $mvResponse = Cache::get('mv_token');
    }

    // Return result
    return $mvResponse;
  }

  /**
  * Fetch orders
  */
  public function fetchOrders()
  {
    // Headers
    $headers = [
      'Content-Type' => 'application/json'
    ];

    // Body
    $body = [
      "token" => Cache::get('mv_token'),
      "requests" => [
        "jsonrpc" => "2.0",
        "method" => "Pedido/find",
        "params" => [
          "estabelecimento_id" => self::ESTABELECIMENTO_ID,
          "cliente_fone" => self::CLIENTE_TELEFONE,
        ],
        "id" => "3226798"
      ],
    ];

    // Send request
    $response = $this->client->post(self::API_URL, [
      'body' => json_encode($body),
      'headers' => $headers
    ]);

    // Return response
    return $mvResponse = json_decode($response->getBody()->getContents(), true);
  }
}
