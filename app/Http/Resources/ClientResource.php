<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'slug' => $this->slug,
      'name' => $this->full_name,
      'first_name' => $this->first_name,
      'last_name' => $this->last_name,
      'phone' => $this->phone,
      'sexo' => $this->sexo,
      'email' => $this->email,
      'cpf' => $this->cpf,
      'avatar' => $this->photo,
      'birthday' => $this->birthday->format('d/m/Y'),
      'salt_points' => $this->refPoints
    ];
  }
}
