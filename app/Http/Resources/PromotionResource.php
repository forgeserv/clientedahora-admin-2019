<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PromotionResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'name' => $this->name,
      'slug' => $this->slug,
      'price' => money($this->price, 'BRL'),
      'descont' => $this->descont,
      'body' => $this->body,
      'date_begin' => $this->date_begin->format('d/m/Y'),
      'date_finish' => $this->date_finish->format('d/m/Y'),
      'category' => $this->category->name,
      'unit' => $this->unit->name,
      'thumbnail' => $this->cover,
    ];
  }
}
