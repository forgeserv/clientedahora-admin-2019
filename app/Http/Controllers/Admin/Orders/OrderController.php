<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Repositories\MVarandas\MVarandasRepository;

class OrderController extends BaseController
{
  use SEOToolsTrait;

  protected $repositories;

  /**
   * Constructor
   */
  public function __construct(MVarandasRepository $repositories)
  {
    // Middlewares
    $this->middleware('permission:view_products', ['only' => ['index']]);

    // Dependency Injection
    $this->repositories = $repositories;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Fetch repositories
    $repository = $this->repositories->fetchOrders();

    dd($repository);

    // Set meta tags
    $this->seo()->setTitle('Pedidos');

    // Return view
    return view('admin.products.index');
  }
}
