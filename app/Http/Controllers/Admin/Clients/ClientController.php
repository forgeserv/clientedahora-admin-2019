<?php

namespace App\Http\Controllers\Admin\Clients;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Client\Client;
use App\Models\Client\ClientAddress;

class ClientController extends BaseController
{
  use SEOToolsTrait;

  private $clients;
  private $address;

  /**
   * Constructor
   */
  public function __construct(Client $clients, ClientAddress $address)
  {
    // Middlewares
    $this->middleware('permission:view_clients', ['only' => ['index']]);
    $this->middleware('permission:add_clients', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_clients', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_clients', ['only' => ['destroy']]);

    // Dependency Injection
    $this->clients = $clients;
    $this->address = $address;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->clients->select('id', 'email', 'first_name', 'last_name', 'cash_total', 'salt_points', 'active')->orderBy('id', 'DESC');

    // Filter by nome param
    if ($request->filled('nome')) {
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    }

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Clientes');

    // Return view
    return view('admin.clients.index', compact('results'));
  }

  public function show($id)
  {
    // Find client
    $result = $this->clients->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Clientes - Visualizar Cliente');

    // Return view
    return view('admin.clients.show', compact('result'));
  }

  public function welcome($id)
  {
    // Find client
    $result = $this->clients->findOrFail($id);

    // Change status
    if ($result->active === false):
      $result->active = true;
      $result->save();
    else:
      // Warning message
      flash('Cliente já está ativo.')->warning();
    endif;

    // Set meta tags
    $this->seo()->setTitle('Cliente - Bem-vindo');

    // Return view
    return view('admin.clients.welcome');
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Set meta tags
    $this->seo()->setTitle('Clientes - Novo Cliente');

    // Return view
    return view('admin.clients.create');
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'email' => "required|email|unique:clients,email",
      'cpf' => "required|unique:clients,cpf",
      'phone' => 'nullable',
      'address.address' => 'nullable|max:255',
      'address.zipcode' => 'nullable|max:255',
      'address.neighborhood' => 'nullable|max:255',
      'address.number' => 'nullable|max:10',
      'address.city' => 'nullable|max:255',
      'address.state' => 'nullable|max:255',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar Cliente.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // If has address
    if (!is_null($request->get('address')['zipcode'])) {
      // Create address
      $addressId = $this->address->create($request->get('address'));

      // Create address_id
      $request->merge(['address_id' => $addressId->id]);
    }

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Hash password
    $request->merge(['password' => bcrypt('clientedahora')]);

    // Create result
    $result = $this->clients->create($request->except('address'));

    // Send Welcome Notification
    $result->sendWelcomeNotification($result);

    // Success message
    flash('Cliente criado com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.clients.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->clients->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Clientes - Editar Cliente');

    // Return view
    return view('admin.clients.edit', compact('result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'email' => "required|email|unique:clients,email,{$id}",
      'cpf' => "required|unique:clients,cpf,{$id}",
      'phone' => 'nullable',
      'address.address' => 'nullable|max:255',
      'address.zipcode' => 'nullable|max:255',
      'address.neighborhood' => 'nullable|max:255',
      'address.number' => 'nullable|max:10',
      'address.city' => 'nullable|max:255',
      'address.state' => 'nullable|max:255',
      'password' => 'nullable|min:8|confirmed',
      'password_confirmation' => 'required_with:password'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar Cliente.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result by id
    $result = $this->clients->findOrFail($id);

    // Fill data
    $result->update($request->except('address'));

    // Save result
    $result->save();

    // Address
    $address = $request->get('address');

    // Update address
    if (!is_null($address['zipcode'])) {
      $result->address()->update($request->get('address'));
    }

    // Success message
    flash('Cliente atualizado com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.clients.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->clients->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Cliente removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Cliente não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Cliente.')->error();

    // Redirect to back page
    return redirect()->back();
  }

   /**
   * Activate the specified resource from storage.
   * @return Response
   */
  public function activate($id)
  {
    // Fetch result by id
    $result = $this->clients->findOrFail($id);

    // Change status
    if ($result->active === false):
      $result->active = true;
      $result->save();

      // Success message
      flash('Cliente ativado com sucesso.')->success();
    else:
      // Warning message
      flash('Cliente já está ativo.')->warning();
    endif;

    // Redirect back
    return redirect()->back();
  }

  /**
  * Deactivate the specified resource from storage.
  * @return Response
  */
  public function deactivate($id)
  {
    // Fetch result by id
    $result = $this->clients->findOrFail($id);

    if($result->id):
      // Change status
      if ($result->active === true):
        $result->active = false;
        $result->save();

        // Success message
        flash('Cliente desativado com sucesso.')->success();
      else:
        // Warning message
        flash('Cliente já está desativado.')->warning();
      endif;
    else:
      // Warning message
      flash('Você não pode desativar seu próprio Cliente.')->error();
    endif;

    // Redirect back
    return redirect()->back();
  }

  public function sendEmail($id)
  {
    // Find result
    $result = $this->clients->findOrFail($id);

    // Send Welcome Notification
    $result->sendWelcomeNotification($result);

    // Success message
    flash('Reenviado e-mail de primeiro acesso')->success();

    // Redirect back
    return redirect()->back();
  }
}
