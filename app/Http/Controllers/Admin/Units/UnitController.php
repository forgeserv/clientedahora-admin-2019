<?php

namespace App\Http\Controllers\Admin\Units;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Unit\Unit;

class UnitController extends BaseController
{
  use SEOToolsTrait;

  private $units;

  /**
   * Constructor
   */
  public function __construct(Unit $units)
  {
    // Middlewares
    $this->middleware('permission:view_units', ['only' => ['index']]);
    $this->middleware('permission:add_units', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_units', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_units', ['only' => ['destroy']]);

    // Dependency Injection
    $this->units = $units;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->units->select('id', 'name', 'active')->orderBy('id', 'DESC');

    // Filter by nome param
    if ($request->filled('nome')) {
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    }

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Unidades');

    // Return view
    return view('admin.units.index', compact('results'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Set meta tags
    $this->seo()->setTitle('Unidade - Nova Unidade');

    // Return view
    return view('admin.units.create');
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar Unidade.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->units->create($request->all());

    // Success message
    flash('Unidade criada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.units.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->units->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Unidade - Editar Unidade');

    // Return view
    return view('admin.units.edit', compact('result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar Unidade.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->units->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Success message
    flash('Unidade atualizada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.units.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->units->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Unidade removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Unidade não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Unidade.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
