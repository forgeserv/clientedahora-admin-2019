<?php

namespace App\Http\Controllers\Admin\Promotions;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Promotion\PromotionCategory;

class PromotionCategoryController extends BaseController
{
  use SEOToolsTrait;

  private $categories;

  /**
   * Constructor
   */
  public function __construct(PromotionCategory $categories)
  {
    // Middlewares
    $this->middleware('permission:view_promotions_submodules', ['only' => ['index']]);
    $this->middleware('permission:add_promotions_submodules', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_promotions_submodules', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_promotions_submodules', ['only' => ['destroy']]);

    // Dependency Injection
    $this->categories = $categories;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Fetch categories
    $results = $this->categories->select('id', 'slug', 'name', 'active')->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle("Promoções - Categorias");

    // Return view
    return view('admin.promotions_categories.index', compact('results'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Set meta tags
    $this->seo()->setTitle("Promoções - Categorias - Nova Categoria");

    // Return view
    return view('admin.promotions_categories.create');
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:promotions_categories'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar Categoria.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->categories->create($request->all());

    // Success message
    flash('Categoria criada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.promotions_categories.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->categories->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle("Promoções - Categorias - Editar Categoria");

    // Return view
    return view('admin.promotions_categories.edit', compact('result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:promotions_categories,name,' . $id,
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar Categoria.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->categories->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Success message
    flash('Categoria atualizada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.promotions_categories.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    if ($request->ajax()):

      // Fetch result
      $result = $this->categories->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Categoria removida com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Categoria não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Categoria.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
