<?php

namespace App\Http\Controllers\Admin\Promotions;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Promotion\Promotion;
use App\Models\Promotion\PromotionCategory;
use App\Models\Unit\Unit;

class PromotionController extends BaseController
{
  use SEOToolsTrait;

  private $promotions;
  private $categories;
  private $units;

  /**
   * Constructor
   */
  public function __construct(Promotion $promotions, PromotionCategory $categories, Unit $units)
  {
    // Middlewares
    $this->middleware('permission:view_promotions', ['only' => ['index']]);
    $this->middleware('permission:add_promotions', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_promotions', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_promotions', ['only' => ['destroy']]);

    // Dependency Injection
    $this->promotions = $promotions;
    $this->categories = $categories;
    $this->units = $units;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->promotions->select('id', 'name', 'active')->orderBy('position', 'ASC');

    // Filter by nome param
    if ($request->filled('nome')) {
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    }

    // Filter by category param
    if ($request->filled('categoria')) {
      $categoria = $request->get('categoria');
      $query->whereHas('category', function ($q) use ($categoria) {
        $q->whereSlug($categoria);
      });
    }

    // Fetch categories filter
    $categories = $this->categories->select('id', 'slug', 'name')->actived()->pluck('name', 'slug');

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Promoções');

    // Return view
    return view('admin.promotions.index', compact('results', 'categories'));
  }

  /**
  * Show the form for creating a new resource.
  * @return Response
  */
  public function create()
  {
    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Fetch units
    $units = $this->units->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Promoções - Nova Promoção');

    // Return view
    return view('admin.promotions.create', compact('categories', 'units'));
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255',
      'price' => 'required',
      'descont' => 'nullable|max:255',
      'date_begin' => 'required|date_format:d/m/Y',
      'date_finish' => 'required|date_format:d/m/Y',
      'body' => 'required',
      'category_id' => 'required',
      'cover' => 'required|image|max:4000',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar Promoção.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->promotions->create($request->all());

    // Request image desktop
    $cover = $request->file('cover');

    // Create cover if send
    if ($cover):
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
              ->usingName($result->name)
              ->usingFileName($filename)
              ->toMediaCollection('promotions');
    endif;

    // Success message
    flash('Promoção criada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.promotions.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->promotions->findOrFail($id);

    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Fetch units
    $units = $this->units->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Promoções - Editar Promoção');

    // Return view
    return view('admin.promotions.edit', compact('result', 'categories', 'units'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
     // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255',
      'price' => 'required',
      'descont' => 'nullable|max:255',
      'date_begin' => 'required|date_format:d/m/Y',
      'date_finish' => 'required|date_format:d/m/Y',
      'body' => 'required',
      'category_id' => 'required',
      'cover' => 'required|image|max:4000',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar Promoção.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->promotions->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Request image desktop
    $cover = $request->file('cover');

    // Upload cover if send
    if ($cover):
      $result->clearMediaCollection('promotions');
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
              ->usingName($result->name)
              ->usingFileName($filename)
              ->toMediaCollection('promotions');
    endif;

    // Success message
    flash('Promoção atualizada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.promotions.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->promotions->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Promoção removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Promoção não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Promoção.')->error();

    // Redirect to back page
    return redirect()->back();
  }

  /**
  * Reorder the specified resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function reorder(Request $request)
  {
    // If ajax
    if ($request->ajax()):

      // Get list
      $list = $request->get('list');

      // Set new order
      $startOrder = 1;

      foreach ($list as $key => $item):
        $promotion = $this->promotions->find($item)->first();
        $promotion->position = $startOrder++;
        $promotion->save();
      endforeach;

      // Return success response
      return response()->json(['success' => true, 'message' => 'Promoções reordenados com sucesso.'], 200);

    else:

      abort(404);

    endif;
  }
}
