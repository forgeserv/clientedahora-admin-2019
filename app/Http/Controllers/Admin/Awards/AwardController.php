<?php

namespace App\Http\Controllers\Admin\Awards;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Award\Award;
use App\Models\Award\AwardCategory;
use App\Models\Unit\Unit;

class AwardController extends BaseController
{
  use SEOToolsTrait;

  private $awards;
  private $categories;
  private $units;

  /**
   * Constructor
   */
  public function __construct(Award $awards, AwardCategory $categories, Unit $units)
  {
    // Middlewares
    $this->middleware('permission:view_awards', ['only' => ['index']]);
    $this->middleware('permission:add_awards', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_awards', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_awards', ['only' => ['destroy']]);

    // Dependency Injection
    $this->awards = $awards;
    $this->categories = $categories;
    $this->units = $units;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->awards->select('id', 'name', 'active')->orderBy('position', 'ASC');

    // Filter by nome param
    if ($request->filled('nome')) {
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    }

    // Filter by category param
    if ($request->filled('categoria')) {
      $categoria = $request->get('categoria');
      $query->whereHas('category', function ($q) use ($categoria) {
        $q->whereSlug($categoria);
      });
    }

    // Fetch categories filter
    $categories = $this->categories->select('id', 'slug', 'name')->actived()->pluck('name', 'slug');

    // Fetch all results
    $results = $query->get();

    // Set meta tags
    $this->seo()->setTitle('Prêmios');

    // Return view
    return view('admin.awards.index', compact('results', 'categories'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Fetch units
    $units = $this->units->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Prêmios - Novo Prêmio');

    // Return view
    return view('admin.awards.create', compact('categories', 'units'));
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255',
      'date_begin' => 'required|date_format:d/m/Y',
      'date_finish' => 'required|date_format:d/m/Y',
      'body' => 'required',
      'stock' => 'required',
      'points' => 'required',
      'category_id' => 'required',
      'cover' => 'required|image|max:4000',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar Prêmio.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->awards->create($request->all());

    // Request image desktop
    $cover = $request->file('cover');

    // Create cover if send
    if ($cover):
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
              ->usingName($result->name)
              ->usingFileName($filename)
              ->toMediaCollection('awards');
    endif;

    // Success message
    flash('Prêmio criada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.awards.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->awards->findOrFail($id);

    // Fetch categories
    $categories = $this->categories->select('id', 'name')->actived()->pluck('name', 'id');

    // Fetch units
    $units = $this->units->select('id', 'name')->actived()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Prêmios - Editar Prêmio');

    // Return view
    return view('admin.awards.edit', compact('result', 'categories', 'units'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
     // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255',
      'points' => 'required',
      'date_begin' => 'required|date_format:d/m/Y',
      'date_finish' => 'required|date_format:d/m/Y',
      'body' => 'required',
      'points' => 'required',
      'category_id' => 'required',
      'cover' => 'nullable|image|max:4000',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao atualizar Prêmio.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->awards->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Request image desktop
    $cover = $request->file('cover');

    // Upload cover if send
    if ($cover):
      $result->clearMediaCollection('awards');
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $result->addMedia($cover)
              ->usingName($result->name)
              ->usingFileName($filename)
              ->toMediaCollection('awards');
    endif;

    // Success message
    flash('Prêmio atualizada com sucesso.')->success();

    // Redirect to list
    return redirect()->route('admin.awards.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->awards->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Prêmio removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Prêmio não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Prêmio.')->error();

    // Redirect to back page
    return redirect()->back();
  }

  /**
  * Reorder the specified resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function reorder(Request $request)
  {
    // If ajax
    if ($request->ajax()):

      // Get list
      $list = $request->get('list');

      // Set new order
      $startOrder = 1;

      foreach ($list as $key => $item):
        $award = $this->awards->find($item)->first();
        $award->position = $startOrder++;
        $award->save();
      endforeach;

      // Return success response
      return response()->json(['success' => true, 'message' => 'Prêmios reordenados com sucesso.'], 200);

    else:

      abort(404);

    endif;
  }
}
