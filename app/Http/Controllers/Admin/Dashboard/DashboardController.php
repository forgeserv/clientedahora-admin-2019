<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Models\Core\Audit;
use Carbon\Carbon;

class DashboardController extends BaseController
{
  use SEOToolsTrait;

  private $audits;

  public function __construct(Audit $audits)
  {
    // Dependency Injection
    $this->audits = $audits;
  }

  /**
   * Return index
   **/
  public function index()
  {
    // Set meta tags
    $this->seo()->setTitle('Dashboard');

    // Return view
    return view('admin.dashboard.index');
  }
}
