<?php

namespace App\Http\Controllers\Admin\Session;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends BaseController
{
  use ResetsPasswords;

  protected $redirectTo = '/admin';

  public function guard()
  {
    return Auth::guard('dashboard');
  }

  public function showResetForm(Request $request, $token = null)
  {
    return view('admin.passwords.reset')->with(
      ['token' => $token, 'email' => $request->email]
    );
  }
}
