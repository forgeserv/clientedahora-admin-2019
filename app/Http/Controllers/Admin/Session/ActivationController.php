<?php

namespace App\Http\Controllers\Admin\Session;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Auth;

class ActivationController extends BaseController
{
  public function showActivationForm()
  {
    // Return view
    return view('admin.passwords.activation');
  }

  public function store(Request $request)
  {
    // Fetch current user
    $user = Auth::guard('dashboard')->user();

    // Validate
    $validate = validator($request->all(), [
      'password' => 'required|min:8|confirmed',
      'password_confirmation' => 'required_with:password'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
      flash('Falha ao adicionar nova senha.')->warning();

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fill data
    $user->fill($request->except('password', 'password_confirmation'));

    // Check password is present
    if($request->filled('password')):
      $user->password = bcrypt($request->get('password'));
    endif;

    // Save user
    $user->save();

    // Success message
    flash('Senha criada com sucesso.')->success();

    // Redirect to profile
    return redirect()->route('admin.dashboard.index');
  }
}
