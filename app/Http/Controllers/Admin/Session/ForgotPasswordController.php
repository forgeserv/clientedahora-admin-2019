<?php

namespace App\Http\Controllers\Admin\Session;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends BaseController
{
  use SendsPasswordResetEmails;

  public function broker()
  {
    return Password::broker();
  }

  public function showLinkRequestForm()
  {
    return view('admin.passwords.forgot');
  }

  public function sendResetLinkEmail(Request $request)
  {
    $this->validateEmail($request);

    $response = $this->broker()->sendResetLink($request->only('email'));

    return $response == Password::RESET_LINK_SENT
        ? $this->sendResetLinkResponse($response)
        : $this->sendResetLinkFailedResponse($request, $response);
  }
}
