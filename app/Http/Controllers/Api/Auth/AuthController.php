<?php

namespace App\Http\Controllers\Api\Auth;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client\Client;

class AuthController extends Controller
{
  private $clients;

  /**
  * Constructor
  */
  public function __construct(Client $clients)
  {
    // Middleware
    // $this->middleware('jwt.auth', ['only' => ['logout']]);

    // Dependency Injection
    $this->clients = $clients;
  }

  public function authenticate(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'cpf' => 'required',
      'password' => 'required'
    ]);

    // If fails validate
    if($validate->fails()):
      return response()->json(['error' => $validate->getMessageBag()], 401);
    endif;

    // Get cpf trim
    $cpf = trim(preg_replace('#[^0-9]#', '', $request->get('cpf')));

    // Find client by cpf
    $client = $this->clients->select('id', 'first_name', 'last_name', 'email', 'cpf', 'phone')->where('cpf', $cpf)->first();

    // If has client
    if ($client) {

      try {

        $credentials = ['email' => $client->email, 'password' => $request->get('password')];

        config()->set('auth.defaults.guard', 'clients');

        if (! $token = JWTAuth::attempt($credentials)) {
          auth()->login($client);
          return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken(auth()->tokenById($client->id));

      } catch (JWTException $e) {
        // something went wrong whilst attempting to encode the token
        return response()->json(['error' => 'Não foi possível criar o token.'], 500);
      }

      // all good so return the token
      return response()->json(compact('token'));
    }
   return response()->json(['error' => 'Cliente não encontrado'], 404);
  }

/**
 * Get the token array structure.
 *
 * @param  string $token
 *
 * @return \Illuminate\Http\JsonResponse
 */
  protected function respondWithToken($token)
  {
    return response()->json([
      'access_token' => $token,
      'token_type' => 'bearer',
      'expires_in' => auth('clients')->factory()->getTTL() * 60
    ]);
  }

  public function refreshToken()
  {
   try {

      $token = JWTAuth::parseToken()->refresh();

			return response()->json([
				'access_token' => $token,
				'expires_in' => 3600
			]);
    } catch (JWTException $exception) {
      return response()->json(['error' => 'token_invalid'],400);
    }
  }

  public function logout(Request $request)
  {
    config()->set('auth.defaults.guard', 'clients');

    auth('clients')->logout();

    JWTAuth::invalidate(JWTAuth::parseToken());

    return response()->json(['message' => 'Sessão finalizada, token invalidado']);
  }
}
