<?php

namespace App\Http\Controllers\Api\Clients;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client\Client;
use App\Models\Award\Award;
use App\Models\Client\ClientAddress;
use App\Models\Auth\User;
use DB;
use App\Http\Resources\ClientResource;
use App\Http\Resources\AwardResource;

class ClientController extends Controller
{
  private $clients;
  private $address;
  private $user;
  private $awards;

  /**
  * Constructors
  */
  public function __construct(Client $clients, ClientAddress $address, Award $awards)
  {
    // Middlewares
    // $this->middleware('jwt.auth', ['only' => ['show', 'update']]);

    // Dependency Injection
    $this->clients = $clients;
    $this->address = $address;
    $this->awards = $awards;

    // Get user token
    $this->user = auth('clients')->user();
  }

  public function avatar(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'avatar' => 'nullable|file|max:6000'
    ]);

    // If fails validate
    if($validate->fails()):
      return response()->json(['error' => $validate->getMessageBag()], 401);
    endif;

    // Request file desktop
    $photo = $request->file('avatar');

    // Create photo if send
    if ($photo):
      $this->user->clearMediaCollection('client_photo');
      $filename = md5($photo->getClientOriginalName()) . '.' . $photo->getClientOriginalExtension();
      $this->user->addMedia($photo)
              ->usingName($this->user->full_name)
              ->usingFileName($filename)
              ->toMediaCollection('client_photo');
    endif;

    // Response
    return response()->json([
      'data' => $this->user->photo,
      'message' => 'Avatar atualizado com sucesso!',
    ], 200);
  }

  public function show(Request $request)
  {
    // Fetch Resrouce
    $data = new ClientResource($this->user);

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso!',
    ], 200);
  }

  public function awards(Request $request)
  {
    // Query init
		$query = $this->user->awards()->orderBy('position', 'ASC');

    // Create resource
    $data = AwardResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function awardsHistory(Request $request)
  {
    // Response
    return response()->json([
      'data' => $this->user->awards,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function store(Request $request)
  {
		// Trim CPF for unique validation
		$request->merge([
			'cpf' => trim(preg_replace('#[^0-9]#', '', $request->get('cpf')))
		]);

    // Validate
    $validate = validator($request->all(),[
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'phone' => 'nullable|max:30',
      'sexo' => 'nullable',
      'avatar' => 'nullable|max:6000',
      'email' => "required|email|unique:clients,email",
      'cpf' => "required|unique:clients,cpf",
      'birthday' => 'nullable|date_format:d/m/Y',
      'password' => 'nullable|min:6',
      // 'password_confirmation' => 'required_with:password',
      // 'address.address' => 'nullable|max:255',
      // 'address.zipcode' => 'nullable|max:255',
      // 'address.neighborhood' => 'nullable|max:255',
      // 'address.number' => 'nullable|max:10',
      // 'address.city' => 'nullable|max:255',
      // 'address.state' => 'nullable|max:255'
    ]);

    // If fails validate
    if($validate->fails()):
      return response()->json(['errors' => $validate->getMessageBag()], 401);
    endif;

    // Begin transaction
    DB::beginTransaction();

    try {
      // If has address
      if ($request->has('address')) {

        // Create address
        $addressId = $this->address->create($request->get('address'));

        // Create address_id
        $request->merge(['address_id' => $addressId->id]);
      }

      // Merge with user_id
      $request->merge(['user_id' => User::find(1)->id]);

      // Create client
      $client = $this->clients->create($request->except('address'));

      // If exists client and token
      if ($client) {

        // Commit
        DB::commit();

        // Request file desktop
        $photo = $request->file('avatar');

        // Create photo if send
        if ($photo):
          $filename = md5($photo->getClientOriginalName()) . '.' . $photo->getClientOriginalExtension();
          $client->addMedia($photo)
                  ->usingName($client->full_name)
                  ->usingFileName($filename)
                  ->toMediaCollection('client_photo');
        endif;

        // Send Welcome Notification
        $client->sendWelcomeNotification($client);

        // Config sets
        config()->set('auth.defaults.guard', 'clients');

        // Response
        return response()->json([
					'access_token' => auth()->tokenById($client->id),
					'token_type' => 'bearer',
					'expires_in' => auth('clients')->factory()->getTTL() * 60
        ], 200);
      }

      // Rollback
      DB::rollback();

      // Something went wrong whilst attempting to encode the token
      return response()->json(['error' => 'Cliente não existe e token não foi criado.'], 500);

    } catch (JWTException $e) {

      // Rollback
      DB::rollback();

      // Something went wrong whilst attempting to encode the token
      return response()->json(['error' => 'Não foi possível criar o token.'], 500);
    }
  }

	public function updatePassword(Request $request)
	{
		$client = $this->user;

		$validate = validator($request->all(), [
			'password' => 'required|min:6|confirmed',
			'password_confirmation' => 'required_with:password'
		]);

		if ($validate->fails()):
			return response()->json(['errors' => $validate->getMessageBag()], 401);
		endif;

		$client->update([
			'password' => $request->get('password')
		]);

		return response()->json([
			'message' => 'Senha atualizada com sucesso!',
		], 200);
	}

  public function update(Request $request)
  {
    // Find client by token
    $client = $this->user;

    // Validate
    $validate = validator($request->all(),[
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'phone' => 'required|max:30',
      'email' => "required|email|unique:clients,email,{$client->id}",
      'birthday' => 'nullable|date_format:d/m/Y',
      // 'cpf' => "required|unique:clients,cpf,{$client->id}",
      // 'sexo' => 'required',
      // 'password' => 'nullable|min:6|confirmed',
      // 'password_confirmation' => 'required_with:password',
      // 'address.address' => 'nullable|max:255',
      // 'address.zipcode' => 'nullable|max:255',
      // 'address.neighborhood' => 'nullable|max:255',
      // 'address.number' => 'nullable|max:10',
      // 'address.city' => 'nullable|max:255',
      // 'address.state' => 'nullable|max:255',
      // 'avatar' => 'nullable|file|max:6000',
    ]);

    // If fails validate
    if($validate->fails()):
      return response()->json(['errors' => $validate->getMessageBag()], 401);
    endif;

    // Begin transaction
    DB::beginTransaction();

    try {

      // Update client
      $client->update($request->except('address'));

      // If has
      if ($request->has('address')) {
        // Update address
        $client->address()->update($request->get('address'));
      }

      // If exists client and token
      if ($client) {

        // Commit
        DB::commit();

        // Request file desktop
        $photo = $request->file('avatar');

        // Create photo if send
        if ($photo):
          $client->clearMediaCollection('client_photo');
          $filename = md5($photo->getClientOriginalName()) . '.' . $photo->getClientOriginalExtension();
          $client->addMedia($photo)
                  ->usingName($client->full_name)
                  ->usingFileName($filename)
                  ->toMediaCollection('client_photo');
        endif;

        // Response
        return response()->json([
          'data' => new ClientResource($client),
          'message' => 'Cliente atualizado com sucesso!',
        ], 200);
      }

      // Rollback
      DB::rollback();

      // something went wrong whilst attempting to encode the token
      return response()->json(['error' => 'Cliente não existe e token não foi criado.'], 500);

    } catch (JWTException $e) {

      // Rollback
      DB::rollback();

      // something went wrong whilst attempting to encode the token
      return response()->json(['error' => 'Não foi possível criar o token.'], 500);
    }
  }
}
