<?php

namespace App\Http\Controllers\Api\Awards;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Award\Award;
use App\Models\Award\AwardCategory;
use DB;
use App\Http\Resources\AwardResource;
use App\Http\Resources\AwardCategoryResource;
use Carbon\Carbon;

class AwardController extends Controller
{
  private $awards;
  private $categories;
  private $user;

  /**
  * Constructor
  */
  public function __construct(Award $awards, AwardCategory $categories)
  {
    // Middlewares
    // $this->middleware('jwt.auth', ['only' => ['clients', 'reedem']]);

    // Get user token
    $this->user = auth('clients')->user();

    // Dependency Injection
    $this->awards = $awards;
    $this->categories = $categories;
  }

  public function index(Request $request)
  {
    // Query init
    $query = $this->awards->orderBy('position', 'ASC');

    // Create resource
    $data = AwardResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function featured(Request $request)
  {
    // Query init
		$query = $this->awards
			->where('release', true)
			->limit(4)
			->orderBy('position', 'ASC');

    // Create resource
    $data = AwardResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function show(Request $request, $id)
  {
    // Result init
    $data = $this->awards->find($id);

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso!',
    ], 200);
  }

  public function categories(Request $request)
  {
    // Query init
    $query = $this->categories->orderBy('id', 'DESC');

    // Create resource
    $data = AwardCategoryResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function awardsByCategory(Request $request, $id)
  {
    // Query init
    $query = $this->awards->byCategory($id)->orderBy('id', 'DESC');

    // Create resource
    $data = AwardResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function redeem(Request $request, $id)
  {
    // Find award by id
    $award = $this->awards->findOrFail($id);

    // Check if has points
    if ($this->user->checkPoints($award->points)) {

      // Substract points
			$this->user->update([
				'salt_points' => $this->user->subtractPoints($award->points)
			]);

      // Points
      $points = [
        $this->user->id => [
          'type' => 2,
          'award_id' => $award->id,
          'points' => $award->points,
          'created_at' => Carbon::now(),
          'active' => true
        ],
      ];

      // Attach awards
      $this->user->awards()->attach($points);

    } else {
      // Response
      return response()->json([
        'message' => 'Você não tem pontos necessários',
      ], 404);
    }

    // Response
    return response()->json([
			'award' => new AwardResource($award),
			'points' => $this->user->refPoints,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }
}
