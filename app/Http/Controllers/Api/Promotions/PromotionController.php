<?php

namespace App\Http\Controllers\Api\Promotions;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promotion\Promotion;
use App\Models\Promotion\PromotionCategory;
use DB;
use App\Http\Resources\PromotionResource;
use App\Http\Resources\PromotionCategoryResource;
use Carbon\Carbon;

class PromotionController extends Controller
{
  private $promotions;
  private $categories;
  private $user;

  /**
  * Constructor
  */
  public function __construct(Promotion $promotions, PromotionCategory $categories)
  {
    // Middlewares
    // $this->middleware('jwt.auth', ['only' => ['clients', 'reedem']]);

    // Get user token
    $this->user = auth('clients')->user();

    // Dependency Injection
    $this->promotions = $promotions;
    $this->categories = $categories;
  }

  public function index(Request $request)
  {
    // Query init
    $query = $this->promotions->orderBy('position', 'ASC');

    // Create resource
    $data = PromotionResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function featured(Request $request)
  {
    // Query init
		$query = $this->promotions
			->where('release', true)
			->limit(4)
			->orderBy('position', 'ASC');

    // Create resource
    $data = PromotionResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function show(Request $request, $id)
  {
    // Result init
    $data = $this->promotions->find($id);

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso!',
    ], 200);
  }

  public function release(Request $request)
  {
    // Query init
    $query = $this->promotions->release()->orderBy('position', 'ASC');

    // Create resource
    $data = PromotionResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function categories(Request $request)
  {
    // Query init
    $query = $this->categories->orderBy('id', 'DESC');

    // Create resource
    $data = PromotionCategoryResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }

  public function promotionsByCategory(Request $request, $id)
  {
    // Query init
    $query = $this->promotions->byCategory($id)->orderBy('id', 'DESC');

    // Create resource
    $data = PromotionResource::collection($query->get());

    // Response
    return response()->json([
      'data' => $data,
      'message' => 'Requisição feita com sucesso',
    ], 200);
  }
}
