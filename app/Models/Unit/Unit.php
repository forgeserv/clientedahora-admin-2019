<?php

namespace App\Models\Unit;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Unit extends Model implements AuditableInterface
{
  use AuditableTrait;
  use Sluggable;

  protected $fillable = [
    'slug',
    'name',
    'active',
    'user_id'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'slug',
    'name',
    'active',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * Scopes
  */
  public function scopeActived($query)
  {
    $query->where('active', true);
  }

  public function scopePosition($query)
  {
    $query->orderBy('position', 'ASC');
  }

  /**
  * Relations
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id')->select('id', 'first_name', 'last_name');
  }
}
