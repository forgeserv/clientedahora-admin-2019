<?php

namespace App\Models\Promotion;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use App\Models\Unit\Unit;
use Carbon\Carbon;

class Promotion extends Model implements HasMedia, AuditableInterface
{
  use AuditableTrait;
  use Sluggable;
  use HasMediaTrait;

  protected $fillable = [
    'slug',
    'name',
    'price',
    'descont',
    'body',
    'position',
    'active',
    'release',
    'date_begin',
    'date_finish',
    'category_id',
    'unit_id',
    'user_id'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  protected $dates = [
    'date_begin',
    'date_finish'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'slug',
    'name',
    'price',
    'descont',
    'body',
    'position',
    'active',
    'release',
    'date_begin',
    'date_finish',
    'category_id',
    'unit_id',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
      $this->addMediaConversion('thumb')
          ->fit(Manipulations::FIT_CROP, 120, 120)
          ->quality(85)
          ->performOnCollections('promotions')
          ->nonQueued();

      $this->addMediaConversion('cover')
          ->fit(Manipulations::FIT_CROP, 120, 120)
          ->quality(85)
          ->performOnCollections('promotions')
          ->nonQueued();
  }

  /**
  * Scopes
  */
  public function scopeActived($query)
  {
    $query->where('active', true);
  }

  public function scopeRelease($query)
  {
    $query->where('release', true);
  }

  public function scopePosition($query)
  {
    $query->orderBy('position', 'ASC');
  }

  public function scopeByCategory($query, $id)
  {
		$query->whereHas('category', function($query) use ($id) {
			return $query->where('id', $id);
		});
  }

  /**
  * Mutators
  */
  public function setDateBeginAttribute($input)
  {
    if($input)
      $this->attributes['date_begin'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  public function setDateFinishAttribute($input)
  {
    if($input)
      $this->attributes['date_finish'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  public function setPriceAttribute($value)
  {
    $this->attributes['price'] = ($value != null) ? str_replace(['.', ','], ['', '.'], $value) : null;
  }

  /**
  * Relations
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id')->select('id', 'first_name', 'last_name');
  }

  public function category()
  {
    return $this->belongsTo(PromotionCategory::class, 'category_id')->select('id', 'name', 'active');
  }

  public function unit()
  {
    return $this->belongsTo(Unit::class, 'unit_id')->select('id', 'name', 'active');
  }

  /**
  * Accesors
  */
  public function getThumbAttribute()
  {
    $image = $this->getMedia('promotions')->first();
    return isset($image) ? $image->getUrl('thumb') : null;
  }

  public function getCoverAttribute()
  {
    $image = $this->getMedia('promotions')->first();
    return isset($image) ? $image->getUrl('cover') : null;
  }
}
