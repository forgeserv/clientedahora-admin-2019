<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
  protected $fillable = [
    'title',
    'key',
    'value',
    'active'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];
}
