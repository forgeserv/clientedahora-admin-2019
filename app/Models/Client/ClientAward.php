<?php

namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

class ClientAward extends Model
{
  protected $table = 'clients_awards';

  protected $fillable = [
    'type',
    'points',
    'active',
    'client_id',
    'award_id'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
  * Mutators
  */
  public function getTypeNameAttribute()
  {
    $value = null;

    switch ($this->type) {
      case 1:
        $value = 'Entrada';
        break;
      case 2:
        $value = 'Saída';
        break;
    }
  }

  /**
  * Relations
  */

  /**
  * Accesors
  */
}
