<?php

namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

class ClientAddress extends Model
{
  protected $table = 'clients_address';

  protected $fillable = [
    'address',
    'zipcode',
    'neighborhood',
    'number',
    'city',
    'state'
  ];

  /**
  * Mutators
  */
  public function setZipCodeAttribute($value)
  {
    $this->attributes['zipcode'] = ($value != null) ? trim(preg_replace('#[^0-9]#', '', $value)) : null;
  }

  /**
  * Relations
  */
  public function client()
  {
    return $this->belongsTo(Client::class, 'address_id');
  }

  /**
  * Accesors
  */
}
