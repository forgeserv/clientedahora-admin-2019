<?php

namespace App\Models\Client;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Auth\User;
use App\Models\Unit\Unit;
use App\Models\Award\Award;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\Clients\Welcome;

class Client extends Authenticatable implements JWTSubject, HasMedia, AuditableInterface
{
  use AuditableTrait, Sluggable, HasMediaTrait, Notifiable;

  protected $guard_name = 'clients';

  protected $fillable = [
    'slug',
    'first_name',
    'last_name',
    'phone',
    'sexo',
    'email',
    'cpf',
    'cash_total',
    'salt_points',
    'birthday',
    'password',
    'receive_messages',
    'termUse',
    'address_id',
    'user_id',
    'active'
  ];

  protected $hidden = [
    'password',
    'remember_token'
  ];

  protected $casts = [
    'receive_messages' => 'boolean',
    'termUse' => 'boolean',
    'active' => 'boolean'
  ];

  protected $dates = [
    'birthday'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'slug',
    'first_name',
    'last_name',
    'phone',
    'sexo',
    'email',
    'cpf',
    'cash_total',
    'salt_points',
    'birthday',
    'password',
    'receive_messages',
    'termUse',
    'address_id',
    'user_id',
    'active'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'full_name'
      ]
    ];
  }

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
    $this->addMediaConversion('client_photo')
         ->fit(Manipulations::FIT_CROP, 320, 320)
         ->quality(90)
         ->nonQueued();
  }

  /**
  * Scopes
  */
  public function scopebyActive($query)
  {
    return $query->where('active', true);
  }

  /**
  * Notifications
  */
  public function sendWelcomeNotification($client)
  {
    $this->notify(new Welcome($client));
  }

  /**
   * Relationships
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function address()
  {
    return $this->belongsTo(ClientAddress::class, 'address_id');
  }

  public function awards()
  {
    return $this->belongsToMany(Award::class, 'clients_awards')->withPivot(['active', 'points']);
  }

  /**
  * Mutators
  */
  public function setEmailAttribute($input)
  {
    if ($input)
      $this->attributes['email'] = mb_strtolower($input, 'UTF-8');
  }

  public function setPhoneAttribute($input)
  {
    if ($input)
      $this->attributes['phone'] = trim(preg_replace('#[^0-9]#', '', $input));
  }

  public function setBirthdayAttribute($input)
  {
    if($input)
      $this->attributes['birthday'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  public function setPasswordAttribute($password)
  {
    $this->attributes['password'] = bcrypt($password);
  }

  public function setCpfAttribute($value)
  {
    $this->attributes['cpf'] = ($value != null) ? trim(preg_replace('#[^0-9]#', '', $value)) : null;
  }

  public function setCashTotalAttribute($value)
  {
    $this->attributes['cash_total'] = ($value != null) ? str_replace(['.', ','], ['', '.'], $value) : null;
  }

  /**
  * Accesors
  */
  public function getPhotoAttribute()
  {
    $image = $this->getMedia('client_photo')->first();
    return isset($image) ? $image->getFullUrl() : asset('/assets/images/faces/default.png');
  }

  public function getFullNameAttribute()
  {
    return "{$this->first_name} {$this->last_name}";
  }

  public function getRefPointsAttribute()
  {
    return str_pad($this->salt_points, 0, STR_PAD_LEFT) . "P";
  }

	public function getBirthdayFormatedAttribute()
	{
		return $this->birthday;
	}

  public function subtractPoints($awards_points)
  {
    return $diff = (int) $this->salt_points - (int) $awards_points;
  }

  public function checkPoints($awards_points)
  {
    if ( (int) $this->salt_points >= (int) $awards_points)
      return true;

    return false;
  }

  /**
  * Get the identifier that will be stored in the subject claim of the JWT.
  *
  * @return mixed
  */
  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  /**
   * Return a key value array, containing any custom claims to be added to the JWT.
   *
   * @return array
   */
  public function getJWTCustomClaims()
  {
    return [];
  }
}


