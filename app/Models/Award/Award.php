<?php

namespace App\Models\Award;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use Carbon\Carbon;
use App\Models\Client\Client;

class Award extends Model implements HasMedia, AuditableInterface
{
  use AuditableTrait;
  use Sluggable;
  use HasMediaTrait;

  protected $fillable = [
    'slug',
    'code',
    'name',
    'position',
    'points',
    'stock',
    'date_begin',
    'date_finish',
    'active',
    'release',
    'body',
    'category_id',
    'unit_id',
    'user_id'
  ];

  protected $casts = [
    'active' => 'boolean',
    'release' => 'boolean'
  ];

  protected $dates = [
    'date_begin',
    'date_finish'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'slug',
    'code',
    'name',
    'position',
    'points',
    'stock',
    'date_begin',
    'date_finish',
    'active',
    'body',
    'release',
    'category_id',
    'unit_id',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model)
    {
      $model->configs();
    });
  }

  /**
  * Configs
  */
  protected function configs()
  {
    // Set code
    $this->attributes['code'] = sha1(date('dmyhi') . $this->name);
  }

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
      $this->addMediaConversion('thumb')
          ->fit(Manipulations::FIT_CROP, 120, 120)
          ->quality(85)
          ->performOnCollections('awards')
          ->nonQueued();

      $this->addMediaConversion('cover')
          ->fit(Manipulations::FIT_CROP, 120, 120)
          ->quality(85)
          ->performOnCollections('awards')
          ->nonQueued();
  }

  /**
  * Scopes
  */
  public function scopeActived($query)
  {
    $query->where('active', true);
  }

  public function scopeRelease($query)
  {
    $query->where('release', true);
  }

  public function scopePosition($query)
  {
    $query->orderBy('position', 'ASC');
  }

  public function scopeByClient($query, $client_id)
  {
    $query->whereHas('clients', function ($q) use ($client_id) {
      $q->where('clients.id', $client_id);
    });
  }

  public function scopeByCategory($query, $category_id)
  {
    $query->whereHas('category', function ($q) use ($category_id) {
      $q->where('id', $category_id);
    });
  }

  /**
  * Mutators
  */
  public function setDateBeginAttribute($input)
  {
    if($input)
      $this->attributes['date_begin'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  public function setDateFinishAttribute($input)
  {
    if($input)
      $this->attributes['date_finish'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  /**
  * Relations
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id')->select('id', 'first_name', 'last_name');
  }

  public function category()
  {
    return $this->belongsTo(AwardCategory::class, 'category_id')->select('id', 'name', 'active');
  }

  public function clients()
  {
    return $this->belongsToMany(Client::class, 'clients_awards')->withPivot(['active', 'points']);
  }

  /**
  * Accesors
  */
  public function getThumbAttribute()
  {
    $image = $this->getMedia('awards')->first();
    return isset($image) ? $image->getUrl('thumb') : null;
  }

  public function getCoverAttribute()
  {
    $image = $this->getMedia('awards')->first();
    return isset($image) ? $image->getUrl('cover') : null;
  }

  public function getReferenceAttribute()
  {
    $plus = $this->id + 1;
    return  str_pad($this->id, 5, '0', STR_PAD_LEFT) . "-" . $plus;
  }

  public function getRefPointsAttribute()
  {
    return str_pad($this->points, 0, STR_PAD_LEFT) . "P";
  }
}
