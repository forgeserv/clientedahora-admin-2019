<?php

namespace App\Notifications\Clients;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Watson\Autologin\Facades\Autologin;

class Welcome extends Notification
{
  use Queueable;

  /**
   * Create a new notification instance.
   *
   * @return void
   */
  public function __construct($client)
  {
    $this->client = $client;
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param  mixed  $notifiable
   * @return array
   */
  public function via($notifiable)
  {
    return ['mail'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param  mixed  $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable)
  {
    $url = route('admin.clients.welcome', ['id' => $this->client->id]);

    return (new MailMessage)
            ->from(config('panel.email.default'), config('app.name'))
            ->subject($this->client->first_name . ' | Bem Vindo')
            ->markdown('admin.notifications.welcome_client', ['client' => $this->client, 'url' => $url]);
  }
}
