# BRZ Boilerplate
HTML5 Twitter Bootstrap 3 Boilerplate with Gulp and ES2015 modules

### How to use

> 1. Run **npm install** to install dev dependencies
> 2. Run **bower install** to install front dependencies
> 3. Run **gulp serve** to work or **gulp** to build
