<?php

return [
  'menu' => [
    [
      'name' => 'Clientes',
      'permission' => 'view_clients',
      'route' => 'admin.clients.index'
    ],
    [
      'name' => 'Promoções',
      'permission' => 'view_promotions',
      'route' => 'admin.promotions.index'
    ],
    [
      'name' => 'Prêmios',
      'permission' => 'view_awards',
      'route' => 'admin.awards.index'
    ],
    [
      'name' => 'Unidades',
      'permission' => 'view_units',
      'route' => 'admin.units.index'
    ],
    [
      'name' => 'Usuários',
      'permission' => 'view_users',
      'route' => 'admin.users.index'
    ],
  ],
  'audit' => [
    'types' => [
      'Client'                               => 'Cliente',
      'Adward'                               => 'Prêmio',
      'Unit'                                 => 'Unidade',
      'Product'                              => 'Produto',
    ],
    'actions' => [
      'created'       => 'Criação',
      'updated'       => 'Alteração',
      'deleted'       => 'Exclusão',
      'restored'      => 'Reativação'
    ],
  ],
  'email' => [
    'default' => 'suporte@qualitare.com.br'
  ]
];
