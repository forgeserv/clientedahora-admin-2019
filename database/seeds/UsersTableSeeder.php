<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\User;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $joao = User::create([
      'first_name' => 'Joao',
      'last_name' => 'Marcus',
      'email' => 'joaomarcusjesus@gmail.com',
      'phone' => null,
      'password' => bcrypt('senha'),
      'receive_messages' => true,
      'active' => true
    ]);

    $joao->assignRole('root');
  }
}
