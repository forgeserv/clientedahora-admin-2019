s<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\Permission;

class PermissionsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Reset cached roles and permissions
    app()['cache']->forget('spatie.permission.cache');

    // Permissions
    $permissions = [

      // Audit
      ['name' => 'view_audits', 'details' => 'Auditorias [view]'],

      // Products
      ['name' => 'view_products', 'details' => 'Produtos [view]'],

      // Awards
      ['name' => 'view_awards', 'details' => 'Prêmios [view]'],
      ['name' => 'add_awards', 'details' => 'Prêmios [add]'],
      ['name' => 'edit_awards', 'details' => 'Prêmios [edit]'],
      ['name' => 'delete_awards', 'details' => 'Prêmios [delete]'],

      // Awards Submodules
      ['name' => 'view_awards_submodules', 'details' => 'Prêmios Submódulos [view]'],
      ['name' => 'add_awards_submodules', 'details' => 'Prêmios Submódulos [add]'],
      ['name' => 'edit_awards_submodules', 'details' => 'Prêmios Submódulos [edit]'],
      ['name' => 'delete_awards_submodules', 'details' => 'Prêmios Submódulos [delete]'],

      // Promotions
      ['name' => 'view_promotions', 'details' => 'Promoções [view]'],
      ['name' => 'add_promotions', 'details' => 'Promoções [add]'],
      ['name' => 'edit_promotions', 'details' => 'Promoções [edit]'],
      ['name' => 'delete_promotions', 'details' => 'Promoções [delete]'],

      // Promotions Submodules
      ['name' => 'view_promotions_submodules', 'details' => 'Promoções Submódulos [view]'],
      ['name' => 'add_promotions_submodules', 'details' => 'Promoções Submódulos [add]'],
      ['name' => 'edit_promotions_submodules', 'details' => 'Promoções Submódulos [edit]'],
      ['name' => 'delete_promotions_submodules', 'details' => 'Promoções Submódulos [delete]'],

      // Clients
      ['name' => 'view_clients', 'details' => 'Clientes [view]'],
      ['name' => 'add_clients', 'details' => 'Clientes [add]'],
      ['name' => 'edit_clients', 'details' => 'Clientes [edit]'],
      ['name' => 'delete_clients', 'details' => 'Clientes [delete]'],

      // Units
      ['name' => 'view_units', 'details' => 'Unidades [view]'],
      ['name' => 'add_units', 'details' => 'Unidades [add]'],
      ['name' => 'edit_units', 'details' => 'Unidades [edit]'],
      ['name' => 'delete_units', 'details' => 'Unidades [delete]'],

      // Permissions
      ['name' => 'view_permissions', 'details' => 'Permissões [view]'],
      ['name' => 'add_permissions', 'details' => 'Permissões [add]'],
      ['name' => 'edit_permissions', 'details' => 'Permissões [edit]'],
      ['name' => 'delete_permissions', 'details' => 'Permissões [delete]'],

      // Roles
      ['name' => 'view_roles', 'details' => 'Grupos [view]'],
      ['name' => 'add_roles', 'details' => 'Grupos [add]'],
      ['name' => 'edit_roles', 'details' => 'Grupos [edit]'],
      ['name' => 'delete_roles', 'details' => 'Grupos [delete]'],

      // Users
      ['name' => 'view_users', 'details' => 'Usuários [view]'],
      ['name' => 'add_users', 'details' => 'Usuários [add]'],
      ['name' => 'edit_users', 'details' => 'Usuários [edit]'],
      ['name' => 'delete_users', 'details' => 'Usuários [delete]'],
    ];

    foreach($permissions as $permission):
      Permission::create([
        'name' => $permission['name'],
        'details' => $permission['details'],
        'guard_name' => 'dashboard'
      ]);
    endforeach;
  }
}
