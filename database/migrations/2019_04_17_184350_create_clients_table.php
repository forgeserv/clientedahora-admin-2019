<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('clients_address', function (Blueprint $table) {
      $table->increments('id');
      $table->string('address');
      $table->string('zipcode');
      $table->string('neighborhood');
      $table->string('complement')->nullable();
      $table->string('number')->nullable();
      $table->string('city');
      $table->string('state');
      $table->timestamps();
    });

    Schema::create('clients', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('first_name');
      $table->string('last_name');
      $table->string('email')->unique();
      $table->string('cpf')->unique();
      $table->string('zipcode')->nullable();
      $table->string('phone')->nullable();
      $table->unsignedInteger('sexo')->default(0);
      $table->unsignedInteger('salt_points')->default(0);
      $table->decimal('cash_total', 10, 2)->default(0);
      $table->date('birthday')->nullable();
      $table->string('password');
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index()->nullable();
      $table->foreign('user_id')->references('id')->on('users');
      $table->unsignedInteger('address_id')->index()->nullable();
      $table->foreign('address_id')->references('id')->on('clients_address');
      $table->boolean('termUse')->nullable()->default(0);
      $table->boolean('receive_messages')->nullable()->default(0);
      $table->timestamps();
    });

    Schema::create('clients_awards', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('type');
      $table->unsignedInteger('points');
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('client_id')->index();
      $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
      $table->unsignedInteger('award_id')->index();
      $table->foreign('award_id')->references('id')->on('awards')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('clients_address');
    Schema::dropIfExists('clients_awards');
    Schema::dropIfExists('clients');
  }
}
