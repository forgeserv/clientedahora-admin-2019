<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwardsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('awards_categories', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('name');
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('awards', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('code')->unique();
      $table->string('name');
      $table->unsignedInteger('stock')->nullable();
      $table->unsignedInteger('position')->nullable();
      $table->unsignedInteger('points')->nullable();
      $table->date('date_begin')->nullable();
      $table->date('date_finish')->nullable();
      $table->boolean('active')->nullable()->default(0);
      $table->boolean('release')->nullable()->default(0);
      $table->longText('body')->nullable();
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->unsignedInteger('unit_id')->index();
      $table->foreign('unit_id')->references('id')->on('units');
      $table->unsignedInteger('category_id')->index();
      $table->foreign('category_id')->references('id')->on('awards_categories');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('awards_categories');
    Schema::dropIfExists('awards');
  }
}
