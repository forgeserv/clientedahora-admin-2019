<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('promotions_categories', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('name');
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('promotions', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug');
      $table->string('name');
      $table->decimal('price', 10, 2);
      $table->string('descont')->nullable();
      $table->longText('body')->nullable();
      $table->date('date_begin')->nullable();
      $table->date('date_finish')->nullable();
      $table->unsignedInteger('position')->nullable();
      $table->boolean('active')->nullable()->default(0);
      $table->boolean('release')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->unsignedInteger('category_id')->index();
      $table->foreign('category_id')->references('id')->on('promotions_categories');
      $table->unsignedInteger('unit_id')->index();
      $table->foreign('unit_id')->references('id')->on('units');
      $table->timestamps();
    });

    Schema::create('clients_promotions', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('promotion_id')->index();
      $table->foreign('promotion_id')->references('id')->on('promotions')->onDelete('cascade');
      $table->unsignedInteger('client_id')->index();
      $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('promotions');
  }
}
